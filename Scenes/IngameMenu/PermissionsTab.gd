extends Tabs

signal request_change_permission_s(role, permission, category, value)

func populate_roles(roles, user_role):
	var role_node = $Role
	role_node.clear()
	
	for role in roles:
		role_node.add_item(role)
	
	if user_role:
		role_node.select_text(user_role)

func get_permission_nodes():
	return $ScrollContainer/Permissions

func request_change_permission(toggle, button):
	var role_name = $Role.get_current_text()
	var category = button.get_parent().get_name()
	var permission = button.get_name()
	var value = button.is_pressed()
	
	emit_signal("request_change_permission_s", role_name, permission, category, value)

func update_permissions():
	var role_name = $Role.get_current_text()
	var role = PermissionManager.get_role(role_name)
	
	for category in role.data.permissions:
		for permission in role.data.permissions[category]:
			get_node("ScrollContainer/Permissions/" + category + "/" + permission).set_pressed(
				role.has_permission(permission, category))

# =================== Helper Functions ===================

func proxy_update_permissions(idx):
	update_permissions()

# =================== Initialization Functions ===================

func connect_buttons():
	var permissions = $ScrollContainer/Permissions
	for category in permissions.get_children():
		for permission in category.get_children():
			if not permission.connect("toggled", self, "request_change_permission", [permission]) == OK:
				print(permission.get_name(), " failed to connect to request_change_permission on PermissionsTab!")
	if not $Role.connect("item_selected", self, "proxy_update_permissions") == OK:
		print("Failed to connect item_selected in PermissionsTab!")

func _ready():
	connect_buttons()