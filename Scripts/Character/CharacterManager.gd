extends Node

# Enum for helping finding files in the char_files array. For instance, CHARACTERS is the first
# item, meaning characters.json. Used like this: char_files[CHARACTER.CHARACTERS] == characters.json
enum CHARACTER { CHARACTERS }
const char_files = ["characters.json"]
var Character = preload("res://Scripts/Character/Character.gd")

var characters = {}
var characters_directory

# =================== Character Data Functions ===================

# Later also only pass characters the user has permission to use.

func get_characters():
	return characters

func get_character(character):
	if not character in characters:
		return null
	return characters[character]

func get_character_frames(character):
	if not character in characters:
		return null
	return characters[character].sprite_frames

func character_exists(character):
	if character in characters:
		return true
	return false

# =================== Expression Data Functions ===================

func get_user_expressions():
	var id = ConnectionManager.get_id()
	var user = ConnectionManager.get_user(id)
	
	var character = get_character(user.get_character())
	var expressions = character.get_expressions()
	
	return expressions

func get_default_user_expression():
	var id = ConnectionManager.get_id()
	var user = ConnectionManager.get_user(id)
	var character = get_character(user.get_character())
	
	if not character:
		return null
	
	return character.get_default_expression()

# =================== Character Folder Setup Functions ===================

func load_characters(main_dir):
	if not setup_directory(main_dir):
		return false
	
	var dir = Directory.new()
	dir.open(characters_directory)
	dir.list_dir_begin()
	
	while true:
		var character = dir.get_next()
		if character == "":
			break
		elif not character.begins_with(".") and dir.current_is_dir():
			load_character(character)
	dir.list_dir_end()

func load_character(character):
	characters[character] = Character.new()
	characters[character].data.name = character
	
	var dir = Directory.new()
	dir.open(characters_directory + character + "/")
	dir.list_dir_begin()
	
	while true:
		var expression_file = dir.get_next()
		if expression_file == "":
			break
		elif expression_file.get_extension() == "png":
			load_expression(character, expression_file)
	dir.list_dir_end()
	characters[character].setup_sprite_frames()

func load_expression(character, expression):
	var regex = RegEx.new()
	regex.compile("^(.*)_\\d+.\\b(png)\\b$")
	
	var expression_animated = regex.search(expression)
	if expression_animated:
		if expression_animated.get_string(1) in characters[character].animated_expressions:
			return
		load_expression_animated(character, expression_animated.get_string(1))
		characters[character].animated_expressions.append(expression_animated.get_string(1))
		characters[character].data.expressions.append(expression_animated.get_string(1))
	else:
		load_expression_static(character, expression.get_basename())
		characters[character].data.expressions.append(expression.get_basename())

# Loads settings from characters.json about all possible characters
func load_characters_settings():
	var file = File.new()
	var loaded_settings = {}
	
	if not file.file_exists(get_directory(CHARACTER.CHARACTERS)):
		file.open(get_directory(CHARACTER.CHARACTERS), File.WRITE)
		file.close()
	
	if not file.open(get_directory(CHARACTER.CHARACTERS), File.READ_WRITE) == OK:
		print("Failed to open character configuration file!")
		return false
	while not file.eof_reached():
		var possible_character = parse_json(file.get_line())
		if not typeof(possible_character) == TYPE_DICTIONARY:
			continue
		if not possible_character.has("name"):
			continue
		
		loaded_settings[possible_character.name] = possible_character
	
	for character in characters:
		if not character in loaded_settings:
			file.store_line(to_json(characters[character].data))
			continue
		
		var keys = characters[character].data.keys()
		if loaded_settings[character].has_all(keys):
			characters[character].data = loaded_settings[character]
	
	file.close()
	return true

# Saves all the character settings
func save_characters_settings():
	var file = File.new()
	if file.file_exists(get_directory(CHARACTER.CHARACTERS)):
		file.open(get_directory(CHARACTER.CHARACTERS), File.READ_WRITE)
	else:
		file.open(get_directory(CHARACTER.CHARACTERS), File.WRITE)
	
	file.seek_end()
	for character in characters:
		file.store_line(to_json(characters[character].data))
	
	file.close()

# =================== Character Loading Functions ===================

func load_expression_static(char_name, expression):
	if not characters.has(char_name):
		return false
	if expression_exists(char_name, expression) and not is_animated(char_name, expression):
		print("Loading expression ", expression, " from character ", char_name, "...")
		var texture = load_frame((get_char_folder(char_name) + expression), null)
		characters[char_name].add_expression(expression, [texture])
		print("Added expression ", expression)
		return true
	return false

func load_expression_animated(char_name, expression):
	if not characters.has(char_name):
		return false
	if expression_exists(char_name, expression) and is_animated(char_name, expression):
		print("Starting to load animated expression ", expression, " from character ", char_name, "...")
		var frame = 1
		var file = File.new()
		var anim = []
		while file.file_exists(get_char_folder(char_name) + expression + "_" + str(frame) + ".png"):
			print("Loading frame ", str(frame), " of ", expression, " for ", char_name, "...")
			var texture = load_frame((get_char_folder(char_name) + expression), frame)
			anim.append(texture)
			frame += 1
			print("Done!")
		
		characters[char_name].add_expression(expression, anim)
		return true
	return false

func load_frame(expression_path, frame):
	var image = Image.new()
	
	if frame:
		image.load(expression_path + "_" + str(frame) + ".png")
	else:
		image.load(expression_path + ".png")
	
	var texture = ImageTexture.new()
	# The flag FLAG_FILTER removes the lingering line but makes downscaled textures much less
	# smooth. Flag number 7 smoothens image but adds line of incorrect alpha on top of the image.
	texture.create_from_image(image, Texture.FLAG_FILTER)
	return texture

# =================== Initializaton Functions ===================

func setup_directory(main_dir):
	if not main_dir:
		return false
	var dir = Directory.new()
	if not dir.dir_exists(main_dir):
		return false
	
	characters_directory = main_dir + "/Characters/"
	if not dir.dir_exists(characters_directory):
		dir.make_dir(characters_directory)
	return true

# =================== Auxiliary Functions ===================

func character_is_valid(character):
	if character.expressions.empty():
		return false
	if character.data.expressions.empty():
		return false
	return true

func get_directory(option):
	if option == CHARACTER.CHARACTERS:
		return characters_directory + char_files[option]
	elif option == null:
		return characters_directory
	
	# Default return
	print("Tried invalid use of get_directory(option) in CharacterManager!")
	return characters_directory

func file_exists(path):
	var file = File.new()
	return file.file_exists(path)

func get_char_folder(char_name):
	return characters_directory + char_name + "/"

func expression_exists(char_name, expression):
	if not expression or not char_name:
		return false
	if not file_exists(get_char_folder(char_name) + expression + ".png"):
		return file_exists(get_char_folder(char_name) + expression + "_1" + ".png")
	return true

func is_animated(char_name, expression):
	return file_exists(get_char_folder(char_name) + expression + "_1" + ".png")