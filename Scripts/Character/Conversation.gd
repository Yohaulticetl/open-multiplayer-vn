extends Resource

# =================== Variables ===================

var name = ""
var positions = {}
var background = "None"

# =================== Actor Functions ===================

func add_actor(actor):
	positions[actor.position] = actor

func get_actors():
	var actors = []
	for position in positions:
		actors.append(positions[position])
	return actors

func get_actor(actor_name):
	for i in positions:
		if positions[i].username == actor_name:
			return positions[i]
	return null

func get_position(pos):
	if pos in positions:
		return positions[pos]
	return null

func get_actor_position(actor):
	for i in positions:
		if positions[i].username == actor.username:
			return i
	return null

func is_actor_in_conversation(actor):
	for i in positions:
		if positions[i].username == actor.username:
			return true
	return false

func is_user_in_conversation(username):
	for i in positions:
		if positions[i].username == username:
			return true
	return false

func is_position_full(pos):
	if get_position(pos):
		return true
	return false

func remove_actor(actor):
	for i in positions:
		if positions[i].username == actor.username:
			positions.erase(i)
			return true
	return false

func is_actor_in_position(actor):
	if typeof(actor.position) == TYPE_INT:
		if actor.position in positions:
			if positions[actor.position].username == actor.username:
				return true
	return false

# =================== Name Functions ===================

func set_name(new_name):
	name = new_name

func get_name():
	return name

# =================== Background Functions ===================

func set_background(bg_name):
	background = bg_name

func get_background():
	return background

# =================== RPC Data Functions ===================

func get_rpc_data():
	var rpc_data = {}
	rpc_data.name = name
	rpc_data.background = background
	rpc_data.positions = {}
	for position in positions:
		rpc_data.positions[position] = positions[position].get_rpc_data()
	return rpc_data

func set_rpc_data(rpc_data, MessageActor):
	name = rpc_data.name
	background = rpc_data.background
	for position in rpc_data.positions:
		var new_actor = MessageActor.new()
		new_actor.set_rpc_data(rpc_data.positions[position])
		add_actor(new_actor)