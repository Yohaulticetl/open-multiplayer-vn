extends Resource

var data = {
	"name" : "",
	"expressions" : [],
}

var expressions = {}
var animated_expressions = []
var sprite_frames = SpriteFrames.new()

# Test debug function. Simply prints each variable's value.
func debug():
	print(data.name)
	print(data.expressions)

# Function validates character to check that everything is set properly.
# Returns false in case something is wrong. True in case not.
func validate_character():
	if data.name == null:
		print("Error: A character has null name.")
		return false
	if data.expressions.empty():
		print("Error: Character ", data.name, " has an empty set of expressions." )
		return false
	
	# Nothing is wrong, return true
	return true

# Creates the dictionary for the expression and sets its frames. To
# be honest, this is here because I was clueless on how to create
# this system at first. It might be used in the future, for adding
# expressions on the fly. That's not the case right now.
func add_expression(expression, frames):
	expressions[expression] = {"frames": frames}

# Appends a frame to the frame array of an expression.
func add_frame(expression, frame):
	expressions[expression]["frames"].append(frame)

# Sets the frames to the frame array of an expression.
func set_frames(expression, frames):
	expressions[expression]["frames"] = frames

# Checks that the key exists in the expression dictionary.
func has_expression(expression):
	return expressions.has(expression)

# Returns the sprite frames for a given expression.
func get_expression_frames(expression):
	if not expressions.has(expression):
		return null
	if not expressions[expression].has("frames"):
		return null
	return expressions[expression]["frames"]

# Returns all valid expressions. Those are the ones that are
# properly registered and have sprite frames.
func get_expressions():
	var valid_expressions = []
	for expression in data.expressions:
		if not expression in expressions:
			continue
		if expressions[expression].empty():
			continue
		valid_expressions.append(expression)
	return valid_expressions

# Gets a default expression. If there is no predefined default,
# it gets the first it finds. If it finds none, then it returns null.
func get_default_expression():
	if data.has("default_expression"):
		if expression_is_valid(data.default_expression):
			return data.default_expression
	elif not data.expressions.empty():
		for expression in data.expressions:
			if expression_is_valid(expression):
				return expression
	return null

func expression_is_valid(expression):
	if not expression in data.expressions:
		return false
	if not expression in expressions:
		return false
	if expressions[expression].empty():
		return false
	return true

# Checks that a key exists, used for loading settings from a json file.
# It normalizes strings if needed.
func validate_setting(dict, key):
	if not dict.has(key):
		return null
	else:
		if typeof(dict[key]) == TYPE_STRING:
			return dict[key].to_lower().capitalize()
		return dict[key]

# Default empty expression loading from file. They're loaded based on the "expressions" key of the
# dictionary. Note that this regards only their names, not the actual images, which are handled in
# the System.gd script file.
func load_expressions(expression_list):
	for expression in expression_list:
		add_expression(expression, [])

# Loads the specific settings from a dictionary. First you have to use getline from a json, though.
func load_settings(settings):
	if not settings:
		return false
	
	data.name = validate_setting(settings, "name")
	
	# Checks existence of a list of expressions, from which it'll search for the appropriate files.
	# Could have been done differently since validate_settings also checks the existence of this key.
	if not settings.has("expressions"):
		return false
	# Will capitalize names and add them to the expressions dictionary as empty dictionaries.
	load_expressions(validate_setting(settings, "expressions"))
	
	if data.name:
		return true
	
	return false

# If attempt exists, loads it into the expression as a setting, otherwise, sets default.
func load_default_expression_setting(expression, attribute, attempt, default):
	if attempt:
		data.expressions[expression][attribute] = attempt
	else:
		data.expressions[expression][attribute] = default

# Uses frames and existent expressions to setup SpriteFrames, which then can be swapped
# in and out of AnimatedSprite to change characters.
func setup_sprite_frames():
	for expression in data.expressions:
		if expression in expressions:
			sprite_frames.add_animation(expression)
			for frame in expressions[expression].frames:
				sprite_frames.add_frame(expression, frame)