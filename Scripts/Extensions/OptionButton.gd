extends OptionButton
var list = []
# Simply put, I have no idea where this variable with all the added items is stored.
# It's easier to just create this list variable now and change it later.

# =================== Position Functions ===================

func populate_positions():
	clear()
	add_item("Far left")
	list.append("Far left")
	
	add_item("Left")
	list.append("Left")
	
	add_item("Center")
	list.append("Center")
	
	add_item("Right")
	list.append("Right")
	
	add_item("Far right")
	list.append("Far right")

func update_position():
	var id = ConnectionManager.get_id()
	var user = ConnectionManager.get_user(id)
	
	var position = user.get_position()
	if position >= 0 and position < get_item_count():
		select(position)

# =================== Expression Functions ===================

func populate_expressions():
	var id = ConnectionManager.get_id()
	var user = ConnectionManager.get_user(id)
	
	clear()
	list.clear()
	
	if not user.get_character():
		return
	
	var expressions = CharacterManager.get_user_expressions()
	for expression in expressions:
		add_item(expression)
		list.append(expression)

func update_expression():
	var id = ConnectionManager.get_id()
	var user = ConnectionManager.get_user(id)
	
	if user.get_expression():
		if user.get_expression() in list:
			select_text(user.get_expression())
			return
	# If character has a default_expression select it now, if not select first item
	var default_expression = CharacterManager.get_default_user_expression()
	
	if default_expression and default_expression in list:
		select_text(default_expression)

# =================== Auxiliar Functions ===================

func select_text(option):
	for idx in range(get_item_count()):
		if get_item_text(idx) == option:
			select(idx)
			return true
	print("An option button failed to find its text ", option)
	return false

func get_current_text():
	if get_item_count() > 0:
		return get_item_text(selected)
	return null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
