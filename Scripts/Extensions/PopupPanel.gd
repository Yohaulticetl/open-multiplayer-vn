tool
extends PopupPanel

# warning-ignore:unused_class_variable
export(String) var text setget set_text, get_text
var click_hides = false

func set_text(new_text):
	$Label.text = new_text

func get_text():
	return $Label.text

func _input(event):
	if not click_hides:
		return
	
	if event.is_action_pressed("mouse_click"):
		click_hides = false
		hide()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
