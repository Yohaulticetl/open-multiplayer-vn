extends Tabs

signal change_character(character)
signal change_expression(expression)
signal change_background(background)
signal change_position(position)
signal change_alias(alias)
signal change_flip_character(flip)
signal change_bgm(bgm)
signal change_sfx(sfx)

# warning-ignore:unused_argument
func request_character_change(idx):
	var character = $Scroll/Children/character.get_current_text()
	if character:
		emit_signal("change_character", character)

# warning-ignore:unused_argument
func request_expression_change(idx):
	var expression = $Scroll/Children/expression.get_current_text()
	if expression:
		emit_signal("change_expression", expression)

# warning-ignore:unused_argument
func request_background_change(idx):
	var background = $Scroll/Children/backgrounds.get_current_text()
	if background:
		emit_signal("change_background", background)

# warning-ignore:unused_argument
func request_position_change(idx):
	var position = $Scroll/Children/position.get_selected()
	if typeof(int(position)) == TYPE_INT:
		emit_signal("change_position", position)

func request_alias_change():
	var alias = $Scroll/Children/alias.get_text()
	if alias:
		emit_signal("change_alias", alias)

func request_flip_character(flip):
	emit_signal("change_flip_character", flip)

func request_bgm_change(idx):
	var bgm = $Scroll/Children/bgm.get_current_text()
	emit_signal("change_bgm", bgm)

func request_sfx_change(idx):
	var sfx = $Scroll/Children/sfx.get_current_text()
	emit_signal("change_sfx", sfx)

# Called when the node enters the scene tree for the first time.
func _ready():
	if not $Scroll/Children/character.connect("item_selected", self, "request_character_change") == OK:
		print("Failed to connect request_character_change button in CharacterTab!")
	if not $Scroll/Children/expression.connect("item_selected", self, "request_expression_change") == OK:
		print("Failed to connect request_expression_change button in CharacterTab!")
	if not $Scroll/Children/backgrounds.connect("item_selected", self, "request_background_change") == OK:
		print("Failed to connect request_background_change in CharacterTab!")
	if not $Scroll/Children/position.connect("item_selected", self, "request_position_change") == OK:
		print("Failed to connect request_position_change in CharacterTab!")
	if not $Scroll/Children/alias/select.connect("button_down", self, "request_alias_change") == OK:
		print("Failed to connect request_alias_change in CharacterTab!")
	if not $Scroll/Children/flip.connect("toggled", self, "request_flip_character") == OK:
		print("Failed to connect request_flip_character in CharacterTab!")
	if not $Scroll/Children/bgm.connect("item_selected", self, "request_bgm_change") == OK:
		print("Failed to connect request_bgm_change in CharacterTab!")
	if not $Scroll/Children/sfx.connect("item_selected", self, "request_sfx_change") == OK:
		print("Failed to connect request_sfx_change in CharacterTab!")
	
	if not connect("change_character", get_parent(), "change_character") == OK:
		print("Failed to connect change_character from CharacterTab!")
	if not connect("change_expression", get_parent(), "change_expression") == OK:
		print("Failed to connect change_expression from CharacterTab!")
	if not connect("change_background", get_parent(), "change_background") == OK:
		print("Failed to connect change_background from CharacterTab!")
	if not connect("change_position", get_parent(), "change_position") == OK:
		print("Failed to connect change_position from CharacterTab!")
	if not connect("change_alias", get_parent(), "change_alias") == OK:
		print("Failed to connect change_alias from CharacterTab!")
	if not connect("change_flip_character", get_parent(), "flip_character") == OK:
		print("Failed to connect change_flip_character from CharacterTab!")
	if not connect("change_bgm", get_parent(), "change_bgm") == OK:
		print("Failed to connect change_bgm from CharacterTab!")
	if not connect("change_sfx", get_parent(), "change_sfx") == OK:
		print("Failed to connect change_sfx from CharacterTab!")
