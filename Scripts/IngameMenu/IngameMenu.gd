extends TabContainer

signal request_change_user_var(target_user, key, value)

signal request_new_conversation(conversation)
signal request_enter_conversation(conversation, position)
signal request_leave_conversation(conversation)
signal request_delete_conversation(conversation)


var characters = {}
var backgrounds = {}
var user

# WARNING: MISSING GET_CURRENT BUTTON TO GET CURRENT BACKGROUND

# =================== Conversation Functions ===================

func setup_conversations(username, conversations):
	$Conversations.username = username
	$Conversations.setup_conversations(conversations)

func update_conversations(conversations):
	$Conversations.update_conversations(conversations)

func new_conversation(conversation_name):
	if conversation_name:
		emit_signal("request_new_conversation", conversation_name)

func enter_conversation(conversation_name, position):
	if conversation_name:
		emit_signal("request_enter_conversation", conversation_name, int(position))

func leave_conversation(conversation_name):
	if conversation_name:
		emit_signal("request_leave_conversation", conversation_name)

func delete_conversation(conversation_name):
	if conversation_name:
		emit_signal("request_delete_conversation", conversation_name)

# =================== Alias Functions ===================

func alias_changed():
	var alias_node = $CharacterSelection/Scroll/Children/alias
	alias_node.set_text(user.get_alias())

func change_alias(alias):
	if not alias:
		alias = null
	if alias == user.get_alias():
		return false
	
	emit_signal("request_change_user_var", null, "alias", alias)
	return true

# =================== Position Functions ===================

func populate_positions():
	var pos_node = $CharacterSelection/Scroll/Children/position
	pos_node.populate_positions()
	position_changed()

func position_changed():
	$CharacterSelection/Scroll/Children/position.update_position()

func change_position(position):
	emit_signal("request_change_user_var", null, "position", int(position))

# =================== Background Functions ===================

func populate_backgrounds():
	var list = BackgroundManager.get_backgrounds()
	var video_list = BackgroundManager.get_videos()
	var bg_node = $CharacterSelection/Scroll/Children/backgrounds
	var conversation_bg = $Conversations/background
	
	bg_node.clear()
	backgrounds.clear()
	conversation_bg.clear()
	
	bg_node.add_item("None")
	conversation_bg.add_item("None")
	
	for background in list:
		if background_is_valid(background):
			backgrounds[background] = list[background]
			bg_node.add_item(background)
			conversation_bg.add_item(background)
	
	bg_node.add_separator()
	conversation_bg.add_separator()
	
	for video in video_list:
		if background_is_valid(video):
			backgrounds[video] = video_list[video]
			bg_node.add_item(video)
			conversation_bg.add_item(video)
	
	var user_bg = user.get_background()
	if user_bg and user_bg in backgrounds:
		bg_node.select_text(user_bg)
	elif not backgrounds.empty():
		bg_node.select(0)

func change_background(background):
	var selection = background
	if not selection in backgrounds:
		selection = null
	
	if selection == user.get_background():
		return false
	emit_signal("request_change_user_var", null, "background", selection)

func background_is_valid(background):
	if BackgroundManager.get_type(background) == "image":
		var bg_res = BackgroundManager.get_background(background)
		if bg_res.get_frames().get_frame_count(bg_res.animation) > 0:
			return true
	elif BackgroundManager.get_type(background) == "video":
		if not BackgroundManager.get_background(background) == null:
			return true
	return false

# =================== Characters Tab Functions ===================

func populate_characters():
	var list = CharacterManager.get_characters()
	var character_node = $CharacterSelection/Scroll/Children/character
	
	character_node.clear()
	characters.clear()
	
	# Also needs to check user permission to use a character
	character_node.add_item("None")
	
	for character in list:
		if CharacterManager.character_is_valid(list[character]):
			characters[character] = list[character]
			character_node.add_item(character)
	
	var user_character = user.get_character()
	if user_character and user_character in characters:
		character_node.select_text(user.get_character())
	elif not characters.empty():
		character_node.select(0)

func populate_expressions():
	var expression_node = $CharacterSelection/Scroll/Children/expression
	
	var id = ConnectionManager.get_id()
	var user = ConnectionManager.get_user(id)
	
	expression_node.populate_expressions()
	expression_node.update_expression()

func change_character(character):
	var selection = character
	if not selection in characters:
		selection = null
	
	if selection == user.get_character():
		return false
	emit_signal("request_change_user_var", null, "character", selection)

func change_expression(expression):
	if expression == user.get_expression():
		return false
	emit_signal("request_change_user_var", null, "expression", expression)

func flip_character(flip):
	if flip == user.get_flip():
		return false
	emit_signal("request_change_user_var", null, "flip_sprite", flip)

func update_flip():
	var flip_node = $CharacterSelection/Scroll/Children/flip
	flip_node.set_pressed(user.get_flip())

func change_bgm(bgm):
	if bgm == user.get_bgm():
		return false
	emit_signal("request_change_user_var", null, "bgm", bgm)

func change_sfx(sfx):
	if sfx == user.get_sfx():
		return false
	emit_signal("request_change_user_var", null, "sfx", sfx)

# =================== Audio Functions ===================

func populate_bgm():
	var bgm_node = $CharacterSelection/Scroll/Children/bgm
	var bgm_dict = AudioManager.get_bgms()
	var conversation_bgm = $Conversations/bgm
	
	bgm_node.clear()
	bgm_node.add_item("None")
	
	conversation_bgm.clear()
	conversation_bgm.add_item("None")
	
	for bgm in bgm_dict:
		if not bgm_dict[bgm]:
			continue
		
		bgm_node.add_item(bgm)
		conversation_bgm.add_item(bgm)
	
	var user_bgm = user.get_bgm()
	if user_bgm and user_bgm in bgm_dict:
		bgm_node.select_text(user_bgm)
	elif not bgm_dict.empty():
		bgm_node.select(0)

func populate_sfx():
	var sfx_node = $CharacterSelection/Scroll/Children/sfx
	var sfx_dict = AudioManager.get_sfxs()
	var conversation_sfx = $Conversations/sfx
	
	sfx_node.clear()
	sfx_node.add_item("None")
	
	conversation_sfx.clear()
	conversation_sfx.add_item("None")
	
	for sfx in sfx_dict:
		if not sfx_dict[sfx]:
			continue
		
		sfx_node.add_item(sfx)
		conversation_sfx.add_item(sfx)
	
	var user_sfx = user.get_sfx()
	if user_sfx and user_sfx in sfx_dict:
		sfx_node.select_text(user_sfx)
	elif not sfx_dict.empty():
		sfx_node.select(0)

# =================== Role Functions ===================

func populate_roles():
	$Permissions.populate_roles(PermissionManager.get_roles(), user.get_role())
	$Permissions.update_permissions()

# =================== Proxy Function for Signal ===================

func user_var_updated(key, value):
	if key == "character":
		populate_characters()
	if key == "expression":
		populate_expressions()
	if key == "background":
		populate_backgrounds()
	if key == "position":
		position_changed()
	if key == "flip":
		update_flip()

# =================== Signal Connection Functions ===================
func connect_to_game():
	# =================== User Variable Signals ===================
	if not connect("request_change_user_var", get_parent(), "request_change_user_var") == OK:
		print("Failed to connect request_change_user_var from the IngameMenu!")
	# =================== Conversation Signals ===================
	if not connect("request_new_conversation", get_parent(), "request_new_conversation") == OK:
		print("Failed to connect request_new_conversation from the IngameMenu!")
	if not connect("request_enter_conversation", get_parent(), "request_enter_conversation") == OK:
		print("Failed to connect request_enter_conversation from the IngameMenu!")
	if not connect("request_leave_conversation", get_parent(), "request_leave_conversation") == OK:
		print("Failed to connect request_leave_conversation from the IngameMenu!")
	if not connect("request_delete_conversation", get_parent(), "request_remove_conversation") == OK:
		print("Failed to connect request_delete_conversation from the IngameMenu!")
	# =================== Parent User Variable Signals ===================
	if not get_parent().connect("user_var_changed", self, "user_var_updated") == OK:
		print("Failed to connect populate_characters from the IngameMenu!")
	# =================== Parent Conversation Signals ===================
	if not get_parent().connect("conversations_changed", self, "update_conversations") == OK:
		print("Failed to connect conversations_changed from the IngameMenu!")
	if not $Conversations.connect("request_update_conversations_s", get_parent(), "update_conversations_menu") == OK:
		print("Failed to connect request_update_conversations_s from the IngameMenu!")
	if not $Conversations.connect("request_change_background_s", get_parent(), "request_change_conversation_bg") == OK:
		print("Failed to connect request_change_background_s from the IngameMenu!")
	# =================== Role Signals ===================
	if not $Permissions.connect("request_change_permission_s", get_parent(), "request_edit_role") == OK:
		print("Failed to connect request_change_permission_s from the IngameMenu!")

# Called when the node enters the scene tree for the first time.
func _ready():
	user = get_parent().get_user(get_parent().get_id())
	populate_characters()
	populate_expressions()
	populate_backgrounds()
	populate_positions()
	populate_roles()
	populate_bgm()
	populate_sfx()
	
	update_flip()
	alias_changed()
	
	connect_to_game()
