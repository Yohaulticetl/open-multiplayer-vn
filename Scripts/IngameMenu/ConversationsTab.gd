extends Tabs

var username
var current_conversation = "None"
var current_background = "None"

signal request_new_conversation_s(conversation_name)
signal request_enter_conversation_s(conversation_name, position)
signal request_leave_conversation_s(conversation_name)
signal request_delete_conversation_s(conversation_name)
signal request_update_conversations_s
signal request_change_background_s(conversation_name, background)

func select_conversation(idx):
	if $selected_conversation.get_current_text() == current_conversation:
		return false
	current_conversation = $selected_conversation.get_current_text()
	emit_signal("request_update_conversations_s")

func select_background(idx):
	var conversation_name = $selected_conversation.get_current_text()
	var bg_name = $background.get_current_text()
	
	if bg_name == current_background:
		return false
	current_background = $background.get_current_text()
	emit_signal("request_change_background_s", conversation_name, bg_name)

func setup_conversations(conversations):
	for i in conversations:
		if conversations[i].is_user_in_conversation(username):
			current_conversation = conversations[i].get_name()
	update_conversations(conversations)

func update_conversations(conversations):
	var conversations_node = $selected_conversation
	conversations_node.clear()
	reset_buttons_name()
	
	conversations_node.add_item("None")
	
	for i in conversations:
		conversations_node.add_item(conversations[i].get_name())
		if conversations[i].get_name() == current_conversation:
			$selected_conversation.select_text(conversations[i].get_name())
			$background.select_text(conversations[i].get_background())
			set_buttons_name($buttons, conversations[i].get_actors())

func delete_conversation():
	if not current_conversation == "None":
		emit_signal("request_delete_conversation_s", current_conversation)

func connect_conversation_buttons(node):
	for i in range(5):
		if not node.get_node(str(i)).connect("button_down", self, "process_button", [node, node.get_node(str(i))]) == OK:
			print("Failed to connect a button for conversation connection in ConversationsTab!")

func process_button(node, button):
	var conversation_name = $selected_conversation.get_current_text()
	if button.get_text() == username:
		emit_signal("request_leave_conversation_s", conversation_name)
	else:
		emit_signal("request_enter_conversation_s", conversation_name, button.get_name())

func request_new_conversation():
	var conversation_name = $new_conversation.get_text()
	var conversation_node = $selected_conversation
	
	for idx in range(conversation_node.get_item_count()):
		if conversation_node.get_item_text(idx) == conversation_name:
			return false
	
	$new_conversation.clear()
	emit_signal("request_new_conversation_s", conversation_name)
	return true

func set_buttons_name(conversation, actors):
	for actor in actors:
		conversation.get_node(str(actor.position)).text = actor.username

func reset_buttons_name():
	get_node("buttons/0").text = "Far Left"
	get_node("buttons/1").text = "Left"
	get_node("buttons/2").text = "Center"
	get_node("buttons/3").text = "Right"
	get_node("buttons/4").text = "Far Right"

func _ready():
	if not $new_conversation/create.connect("button_down", self, "request_new_conversation") == OK:
		print("Failed to connect button for new conversations in ConversationsTab!")
	if not $selected_conversation.connect("item_selected", self, "select_conversation") == OK:
		print("Failed to connect select_conversation in ConversationsTab!")
	if not $selected_conversation/delete.connect("button_down", self, "delete_conversation") == OK:
		print("Failed to connect request_delete_conversation_s in the ConversationsTab!")
	if not $background.connect("item_selected", self, "select_background") == OK:
		print("Failed to connect select_background in the ConversationsTab!")
	
	if not connect("request_new_conversation_s", get_parent(), "new_conversation") == OK:
		print("Failed to connect request_new_conversation_s from the ConversationsTab!")
	if not connect("request_enter_conversation_s", get_parent(), "enter_conversation") == OK:
		print("Failed to connect request_enter_conversation_s from the ConversationsTab!")
	if not connect("request_leave_conversation_s", get_parent(), "leave_conversation") == OK:
		print("Failed to connect request_leave_conversation_s from the ConversationsTab!")
	if not connect("request_delete_conversation_s", get_parent(), "delete_conversation") == OK:
		print("Failed to connect request_delete_conversation_s from the ConversationsTab!")
	
	connect_conversation_buttons($buttons)