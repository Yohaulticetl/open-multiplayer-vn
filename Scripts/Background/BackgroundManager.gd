extends Node

var Background = load("res://Scripts/Background/Background.gd")

# Works the same as the CharacterManager enum. Helps finding specific files in the bg_files array.
enum BACKGROUND { BACKGROUNDS }
var bg_files = ["backgrounds.json"]

var backgrounds = {}
var videos = {}
var backgrounds_directory
var videos_directory

var animated_backgrounds = []
const bg_extensions = ["jpg", "png", "bmp"]
const video_extensions = ["webm", "ogv"]

# Important note: backgrounds, as opposed to characters, are separated into static, animated and video.

# =================== Character Data Functions ===================

func get_backgrounds():
	return backgrounds

func get_videos():
	return videos

func get_background(background):
	if background in backgrounds:
		return backgrounds[background]
	elif background in videos:
		return videos[background]
	return null

func get_background_frames(background):
	if background in backgrounds:
		return backgrounds[background].get_frames()
	elif background in videos:
		return videos[background]
	return null

func background_exists(background):
	if background in backgrounds:
		return true
	elif background in videos:
		return true
	return false

func get_type(background):
	if background in backgrounds:
		return "image"
	elif background in videos:
		return "video"
	return null

# =================== Background Setup ===================

func setup_background(bg_name, extension):
	var background = Background.new()
	background.set_name(bg_name)
	background.set_extension("." + extension)
	background.setup_frames()
	
	backgrounds[background.get_name()] = background
	return background.get_name()

# Function checks which file in directory should be loaded and calls load_background which
# then checks if it's supposed to be animated or not, then calling the appropriate function
# to load resource.
func load_backgrounds(main_dir):
	if not setup_directory(main_dir):
		return false
	
	var dir = Directory.new()
	dir.open(backgrounds_directory)
	dir.list_dir_begin()
	
	while true:
		var file = dir.get_next()
		if file == "":
			break
		
		if file.get_extension() in bg_extensions:
			load_background(file)
	dir.list_dir_end()

# Checks whether background is animated or not through regex and selects
# appropriate function.
func load_background(bg_filename):
	var regex = RegEx.new()
	regex.compile("^(.*)_\\d+.\\b" + get_regex_group(bg_extensions) +"\\b$")
	# Not trying to communicate with aliens that mess with your brain and turn you
	# into a murderous maniac, .* means any character \\d+ or \d+ means one or more
	# integers, \\b are separators. ^ is the start, $ is the end. So, match anything
	# that has an _ then a number, ending with .png, .bmp or .jpg. Stuff in parenthesis
	# are groups, which can get from search. 0 is the whole string though.
	
	var bg_animated = regex.search(bg_filename)
	if bg_animated:
		if bg_animated.get_string(1) in animated_backgrounds:
			return
		var bg_name = setup_background(bg_animated.get_string(1), bg_animated.get_string(2))
		load_background_animated(bg_name)
		animated_backgrounds.append(bg_name)
	else:
		var bg_name = setup_background(bg_filename.get_basename(), bg_filename.get_extension())
		load_background_static(bg_name)

# =================== Loading Backgrounds ===================

func load_background_static(bg_name):
	if not bg_name in backgrounds or is_animated(bg_name, backgrounds[bg_name].get_extension()):
		return false
	
	if bg_exists(bg_name, backgrounds[bg_name].get_extension()):
		var image = Image.new()
		image.load(get_static_bg_path(bg_name, backgrounds[bg_name].get_extension()))
		var texture = ImageTexture.new()
		texture.create_from_image(image)
		
		backgrounds[bg_name].add_frame(texture)
#		print("Added background ", bg_name)
		return true
	return false

func load_background_animated(bg_name):
	if not bg_name in backgrounds or not is_animated(bg_name, backgrounds[bg_name].get_extension()):
		return false
	
#	print("Loading frames for background ", bg_name)
	var frame = 1
	while bg_frame_exists(bg_name, frame):
		var image = Image.new()
		image.load(get_animated_bg_path(bg_name, frame, backgrounds[bg_name].get_extension()))
		var texture = ImageTexture.new()
		texture.create_from_image(image)
		
		backgrounds[bg_name].add_frame(texture)
#		print("Added frame ", frame, "...")
		frame += 1
	
	if not backgrounds[bg_name].empty():
		return true
	return false

func find_bg_extension(bg_name):
	for extension in bg_extensions:
		if bg_exists(bg_name, extension):
			return extension
	return null

# =================== Video Loading Functions ===================

func load_videos():
	if not videos_directory:
		return
	
	var dir = Directory.new()
	dir.open(videos_directory)
	dir.list_dir_begin()
	
	while true:
		var file = dir.get_next()
		if file == "":
			break
		
		if not file.begins_with(".") and file.get_extension() in video_extensions:
			load_video(file)
	dir.list_dir_end()

func load_video(file):
	if not file:
		return false
	
	if video_exists(file):
		var video = load(get_video_path(file))
		videos[file.get_basename()] = video
	return true

# =================== Initializaton Functions ===================

func setup_directory(main_dir):
	if not main_dir:
		return false
	var dir = Directory.new()
	if not dir.dir_exists(main_dir):
		return false
	
	backgrounds_directory = main_dir + "/Backgrounds/"
	videos_directory = main_dir + "/Videos/"
	if not dir.dir_exists(backgrounds_directory):
		dir.make_dir(backgrounds_directory)
	if not dir.dir_exists(videos_directory):
		dir.make_dir(videos_directory)
	return true

# =================== Auxiliary Functions ===================

func is_valid_image_bg(file_name):
	if file_name.get_extension() in bg_extensions:
		return true
	return false

func get_directory(option):
	if option == BACKGROUND.BACKGROUNDS:
		return backgrounds_directory + bg_files[option]
	elif option == null:
		return backgrounds_directory
	
	# Default return
	print("Tried invalid use of get_directory(option) in CharacterManager!")
	return backgrounds_directory

func get_regex_group(group):
	var result = "("
	for i in group:
		result += i + "|"
	result.rstrip("|")
	result += ")"
	
	return result

func get_static_bg_path(bg_name, extension):
	return backgrounds_directory + bg_name + extension

func get_animated_bg_path(bg_name, frame, extension):
	return backgrounds_directory + bg_name + "_" + str(frame) + extension

func get_video_path(video):
	return videos_directory + video

func file_exists(path):
	var file = File.new()
	return file.file_exists(path)

func bg_exists(bg_name, extension):
	if is_animated(bg_name, extension):
		return true
	return file_exists(get_static_bg_path(bg_name, extension))

func video_exists(video):
	return file_exists(get_video_path(video))

func bg_frame_exists(bg_name, frame):
	return file_exists(get_animated_bg_path(bg_name, frame, backgrounds[bg_name].get_extension()))

func is_animated(bg_name, extension):
	return file_exists(get_animated_bg_path(bg_name, 1, extension))