extends Resource

var data = {
	"name" : ""
}
const animation = "default"

var extension = ".png"
var sprite_frames = SpriteFrames.new()


func set_name(name):
	data.name = name

func get_name():
	return data.name


func setup_frames():
	if not sprite_frames.has_animation(animation):
		sprite_frames.add_animation(animation)

func get_frames():
	return sprite_frames

func add_frame(frame):
	sprite_frames.add_frame(animation, frame)


func set_extension(ext):
	if ext:
		extension = ext

func get_extension():
	return extension