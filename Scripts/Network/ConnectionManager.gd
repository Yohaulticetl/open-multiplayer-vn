extends Node

# Check signals
signal check_user_authentication_s(id, username, password)
signal check_register_user_s(id, username, password)

# Variable signals
signal save_blacklist_s(list)
signal load_blacklist_s

var blacklist = []
var logged_users = {}
var random_IDs = {}
var connection_status = 0
var unique_id = ""

signal login_ok_s
signal register_ok_s

const server_username = "[Server]"

# ============== Functions for updating local variables ==============

func update_blacklist(list):
	blacklist = list

func add_to_blacklist(ip):
	if not ip:
		return false
	
	blacklist.append(ip)
	emit_signal("save_blacklist_s", blacklist)
	return true

func remove_from_blacklist(ip):
	if not ip:
		return false
	
	blacklist.erase(ip)
	emit_signal("save_blacklist_s", blacklist)
	return true

func clear_blacklist():
	blacklist = []
	emit_signal("save_blacklist_s", blacklist)

func request_blacklist():
	emit_signal("load_blacklist_s")

# Following function creates a unit of random id, generated
# anew every time the game is opened.
func create_id_unit():
	var id_unit = ""
# warning-ignore:unused_variable
	for x in range(5):
		id_unit += String(randi()%16)
	
	return id_unit

# This function creates a unique random id, for use each
# time the client wants to communicate with the server,
# ensuring the id sent is his.
func create_unique_id():
	var unique_id = ""
	unique_id += create_id_unit()
	
# warning-ignore:unused_variable
	for x in range(4):
		unique_id += "-"
		unique_id += create_id_unit()
	
	return unique_id

# ============== Functions for sending info to server/client ==============

func send_login_info(username, password, server_password):
	if connection_status == 2:
		print("Sending login request")
		rpc_id(1, "check_login", username, password, str(server_password.hash()), get_tree().get_network_unique_id(), unique_id)

func send_register_user(username, password, server_password):
	if connection_status == 2:
		rpc_id(1, "check_register_user", username, password, str(server_password.hash()), get_tree().get_network_unique_id(), unique_id)

func send_login_ok(id, result, user):
	if connection_status == 2:
		rpc_id(id, "receive_login_ok", result, 1, random_IDs[id], user)

func send_register_ok(id, result, user):
	if connection_status == 2:
		rpc_id(id, "receive_register_ok", result, 1, random_IDs[id], user)

# ============== Functions for checking info on server ==============

remote func check_login(username, password, server_password, id, random_id):
	if get_peer().get_peer_address(id) in blacklist and random_IDs[id] == random_id:
		send_login_ok(id, false, null)
		get_peer().disconnect_peer(id)
		return false
	
	print("Checking login...")
	
	if not id in logged_users:
		emit_signal("check_user_authentication_s", id, username, password, server_password)

remote func check_register_user(username, password, server_password, id, random_id):
	if get_peer().get_peer_address(id) in blacklist and random_IDs[id] == random_id:
		send_register_ok(id, false, null)
		get_peer().disconnect_peer(id)
		return false
	
	print("Checking register...")
	if username == server_username:
		send_register_ok(id, false, null)
		get_peer().disconnect_peer(id)
		return false
	
	if not id in logged_users:
		emit_signal("check_register_user_s", id, username, password, server_password)

# ============== Functions for processing signals ==============

func login_ok(id, result, user):
	send_login_ok(id, result, user.get_rpc_data())
	print("Login: ", result)
	if result:
		logged_users[id] = user
	else:
		get_peer().disconnect_peer(id)
	print(id)

func register_ok(id, result, user):
	if user:
		send_register_ok(id, result, user.get_rpc_data())
	else:
		send_register_ok(id, result, user)
	print("Register: ", result)
	if result:
		logged_users[id] = user
	else:
		get_peer().disconnect_peer(id)

func login_server_user(user):
	logged_users[user.id] = user

# ============== Functions for receiving signals from server ==============

remote func receive_login_ok(result, id, random_id, user):
	if id == 1 and random_IDs[get_tree().get_network_unique_id()] == random_id and result:
		var my_user = User.new()
		my_user.set_rpc_data(user)
		
		logged_users[get_tree().get_network_unique_id()] = my_user
	emit_signal("login_ok_s", result)

remote func receive_register_ok(result, id, random_id, user):
	if id == 1 and random_IDs[get_tree().get_network_unique_id()] == random_id and result:
		var my_user = User.new()
		my_user.set_rpc_data(user)
		
		logged_users[get_tree().get_network_unique_id()] = my_user
	emit_signal("register_ok_s", result)

# ============== Periodically updates client connection ==============

func check_connection():
	if get_tree().has_meta("network_peer"):
		# Check if it's a server or not.
		if not get_tree().is_network_server():
			connection_status = get_tree().get_meta("network_peer").get_connection_status()
		else:
			connection_status = 2
	
	# Has no peer set up.
	else:
		connection_status = 0

# ============== Storage for random unique IDs ==============

remote func store_random_id(id, random_id):
	if not id in random_IDs:
		random_IDs[id] = random_id
	
	if get_tree().is_network_server():
		rpc_id(id, "store_random_id", id, unique_id)

func send_random_id():
	rpc_id(1, "store_random_id", get_tree().get_network_unique_id(), unique_id)

func remove_random_id(id):
	if id in logged_users:
		logged_users.erase(id)
	if id in random_IDs:
		random_IDs.erase(id)

# Called when the node enters the scene tree for the first time.
func _ready():
	var connection_timer = Timer.new()
	connection_timer.set_wait_time(5)
	connection_timer.set_one_shot(false)
	connection_timer.connect("timeout", self, "check_connection")
	connection_timer.start()
	add_child(connection_timer)
	
	if get_tree().connect("connected_to_server", self, "check_connection") != 0:
		print("Error connecting check_connection function on the Connection class under _ready()!")
	if get_tree().connect("connected_to_server", self, "send_random_id") != 0:
		print("Error connecting check_connection function to send_random_id()!")
	if get_tree().connect("network_peer_disconnected", self, "remove_random_id") != 0:
		print("Error connecting remove_random_id function to network_peer_disconnected!")
	if get_tree().connect("server_disconnected", self, "check_connection") != 0:
		print("Error connecting server_disconnected signal on the Connection class under _ready()!")
	
	unique_id = create_unique_id()

# ============== Auxiliar Functions ==============

func get_user(id):
	if id in logged_users:
		return logged_users[id]
	return null

func get_users():
	return logged_users

func get_id():
	return get_tree().get_network_unique_id()

func get_random_id(id):
	if id == get_id():
		return unique_id
	return random_IDs[id]

func is_user(id, random_id):
	if not random_id == get_random_id(id):
		return false
	return true

func get_peer():
	return get_tree().get_meta("network_peer")