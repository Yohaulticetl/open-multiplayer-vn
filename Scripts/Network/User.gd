extends Resource
class_name User

# Base class for users, for storing the username, password, IP address
# from which he logged in and permissions he has.

var id = 0
var data = {
	"username" : "",
	"password" : "",
	"address" : "",
	"role" : "Guest"
}

var rpc_data = {
	"username" : "",
	"alias" : "",
	"character" : null,
	"expression" : null,
	"position" : 2,
	"background" : null,
	"scale_sprite" : true,
	"center_sprite" : true,
	"flip_sprite" : false,
	"state" : null,
	"bgm" : null,
	"sfx" : null
}

# =================== Setters/Getters ===================
# Don't recommend people do as I do. Using the getters and
# setters is probably safer. That being said, I'll be saving
# up on lines of code by directly changing the user variables.
# Should still use the getters, though.

# General:

func set_variable(key, value):
	if not key in rpc_data:
		return false
	
	rpc_data[key] = value
	return true

func get_variable(key):
	if not key in rpc_data:
		return null
	
	return rpc_data[key]

# Data:

# Duplicated for now, easier to setup what
# should be sent and what should stay only
# within the server.
func get_username():
	return rpc_data.username

func set_username(username):
	data.username = username
	rpc_data.username = username

# ======================================

func get_password():
	return data.password

func set_password(password):
	data.password = password

# ======================================

func get_address():
	return data.address

func set_address(address):
	data.address = address

# ======================================

func get_role():
	return data.role

func set_role(role):
	data.role = role

# RPC Data:

func get_alias():
	return rpc_data.alias

func set_alias(alias):
	rpc_data.alias = alias

# ======================================

func get_character():
	return rpc_data.character

func set_character(character):
	rpc_data.character = character

# ======================================

func get_expression():
	return rpc_data.expression

func set_expression(expression):
	rpc_data.expression = expression

# ======================================

func get_position():
	return rpc_data.position

func set_position(position):
	rpc_data.position = position

# ======================================

func get_background():
	return rpc_data.background

func set_background(background):
	rpc_data.background = background

# ======================================

func get_scale():
	return rpc_data.scale_sprite

func set_scale(scale):
	rpc_data.scale_sprite = scale

# ======================================

func get_center():
	return rpc_data.center_sprite

func set_center(center):
	rpc_data.center_sprite = center

# ======================================

func get_flip():
	return rpc_data.flip_sprite

func set_flip(flip):
	rpc_data.flip_sprite = flip

# ======================================

func get_state():
	return rpc_data.state

func set_state(state):
	rpc_data.state = state

# ======================================

func get_bgm():
	return rpc_data.bgm

func set_bgm(bgm):
	rpc_data.bgm = bgm

# ======================================

func get_sfx():
	return rpc_data.sfx

func set_sfx(sfx):
	rpc_data.sfx = sfx

# =================== Communication Functions ===================

func get_rpc_data():
	return rpc_data

func set_rpc_data(new_rpc_data):
	if new_rpc_data:
		rpc_data = new_rpc_data