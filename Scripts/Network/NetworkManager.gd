extends Node

const type = "SERVER"
var directory = ""
var config = {}
var server_password = ""
enum files_enum {USERS, SERVER_CONFIG, CLIENT_CONFIG, BLACKLIST}
const files = ["users.json", "server_config.json", "client_config.json", "blacklist.json"]

var connected_to_connection_manager = false

const directory_error = "Directory does not exist. Click anywhere to continue..."
const peer_error = "Failed to create server. Check that a server is not already running. Click anywhere to continue..."

const connect_issue = "Could not connect to server, check proper IP and port. Click anywhere to continue..."
const directory_issue = "Invalid directory. Check that it exists and try again. Click anywhere to continue..."

signal connect_to_server_ok_s(result, text)

# Result signals
signal login_ok_s(id, result, username)
signal register_ok_s(id, result, username)
signal create_server_ok_s(result, text, user)
signal send_blacklist_s(list)
signal server_user_created(user)

# Creates a new server, on a specific server folder.
func create_server(target_dir, port, max_players, password):
	if not target_dir:
		emit_signal("create_server_ok_s", false, directory_error)
		return false
	
	var peer = NetworkedMultiplayerENet.new()
	if peer.create_server(port, max_players) != 0:
		print("Error creating the server from peer.")
		emit_signal("create_server_ok_s", false, peer_error)
		return false
	get_tree().set_network_peer(peer)
	get_tree().set_meta("network_peer", peer)
	directory = target_dir
	
	if password:
		server_password = str(password.hash())
	
	load_blacklist()
	
	var server_user = create_server_user()
	
	# Note: This way of doing things is simply for debugging,
	# wait for proper signals that show the server user was
	# really created before emitting create_server_ok_s
	
	emit_signal("server_user_created", server_user)
	emit_signal("create_server_ok_s", true, null)
	return true

# Closes connections.
func close_server():
	var peer = get_tree().get_meta("network_peer")
	peer.close_connection()
	server_password = ""

# Saves the current server configuration.
func save_config():
	var config_file = File.new()
	config_file.open(directory + "/" + files[files_enum.SERVER_CONFIG], File.WRITE)
	
	config_file.store_line(to_json(config))
	config_file.close()

# =============== Server Connection Functions ===============
func setup_connection_signals(node):
	if not connected_to_connection_manager:
		if node.connect("check_register_user_s", self, "register_user") != 0:
			print("Failed to connect signal for registering users!")
			return false
		if node.connect("check_user_authentication_s", self, "attempt_login") != 0:
			print("Failed to connect signal for users trying to log in!")
			return false
		if connect("register_ok_s", node, "register_ok") != 0:
			print("Failed to connect signal for sending result of registering users!")
			return false
		if connect("login_ok_s", node, "login_ok") != 0:
			print("Failed to connect signal for sending result of user login!")
			return false
		if connect("send_blacklist_s", node, "update_blacklist") != 0:
			print("Failed to connect signal for sending loaded blacklist to Connection!")
			return false
		if node.connect("load_blacklist_s", self, "load_blacklist") != 0:
			print("Failed to connect signal to request loading a blacklist!")
			return false
		if node.connect("save_blacklist_s", self, "save_blacklist") != 0:
			print("Failed to connect signal for saving current blacklist!")
			return false
		if connect("server_user_created", node, "login_server_user") != 0:
			print("Failed to connect signal for logging in server user!")
			return false
		
		connected_to_connection_manager = true
	return true

func setup_server_signals(node):
	if node.connected_to_network_base:
		return
	
	if node.connect("attempt_create_server", self, "create_server") != 0:
		print("Failed to connect signal for creating a server!")
	
	if connect("create_server_ok_s", node, "check_errors") != 0:
		print("Failed to connect server creation result signal!")
	node.connected_to_network_base = true

# =============== Server-Client Interaction Functions ===============

# Registers a new user, fails if the user already exists.
func register_user(id, username, password, srv_pass):
	if not srv_pass == server_password and server_password:
		emit_signal("register_ok_s", id, false, null)
		return false
	
	var users_file = File.new()
	if users_file.file_exists(directory + "/" + files[files_enum.USERS]):
		users_file.open(directory + "/" + files[files_enum.USERS], File.READ_WRITE)
		while not users_file.eof_reached():
			var line = parse_json(users_file.get_line())
			if not line:
				break
			if line.username == username:
				emit_signal("register_ok_s", id, false, null)
				users_file.close()
				return false
	else:
		users_file.open(directory + "/" + files[files_enum.USERS], File.WRITE)
	
	var user = User.new()
	user.set_username(username)
	user.set_password(password)
	user.set_address(get_tree().get_meta("network_peer").get_peer_address(id))
	
	users_file.seek_end()
	users_file.store_line(to_json(user.data))
	users_file.close()
	emit_signal("register_ok_s", id, true, user)
	return true

# Attempts to verify a users's login information.
func attempt_login(id, username, password, srv_pass):
	if not srv_pass == server_password and server_password:
		emit_signal("login_ok_s", id, false, null)
		return false
	
	var users_file = File.new()
	if users_file.open(directory + "/" + files[files_enum.USERS], File.READ) != 0:
		print("Failed to load the users file in the register_user function on Server!")
		return false
	while not users_file.eof_reached():
		var line = parse_json(users_file.get_line())
		if not line:
			emit_signal("login_ok_s", id, false, null)
			return
		if line.username == username:
			if line.password == password:
				var user = User.new()
				user.set_username(username)
				# Not setting up user password to not send it over the network,
				# Don't forget not to save it while this is like that.
				user.set_address(get_tree().get_meta("network_peer").get_peer_address(id))
				
				emit_signal("login_ok_s", id, true, user)
				return
			
			emit_signal("login_ok_s", id, false, null)
			return

# =============== Blacklist Functions ===============
func save_blacklist(list):
	if not list:
		return false
	
	var blacklist_file = File.new()
	if blacklist_file.open(directory + "/" + files[files_enum.BLACKLIST], File.WRITE) != 0:
		print("Failed to open the blacklist file for writing, check permissions!")
		return false
	
	blacklist_file.store_line(to_json(list))
	blacklist_file.close()
	return true

func load_blacklist():
	var blacklist_file = File.new()
	if not blacklist_file.file_exists(directory + "/" + files[files_enum.BLACKLIST]):
		return false
	
	if blacklist_file.open(directory + "/" + files[files_enum.BLACKLIST], File.READ) != 0:
		print("Failed to load the blacklist file in the load_blacklist function of Server!")
		return false
	
	var blacklist = []
	
	while not blacklist_file.eof_reached():
		var line = parse_json(blacklist_file.get_line())
		if line and typeof(line) == TYPE_ARRAY:
			blacklist = line
			break
	
	blacklist_file.close()
	
	emit_signal("send_blacklist_s", blacklist)
	return true

# =============== Auxiliary Functions ===============
func create_server_user():
	var user = User.new()
	user.id = 1
	user.set_username("[Server]")
	user.set_address("127.0.0.1")
	user.set_role("Server")
	return user

# ============================================================
# Client
# ============================================================

# Attempts connection to server using specified arguments.
func connect_to_server(target_dir, port, server_address):
	if not target_dir:
		emit_signal("connect_to_server_ok_s", false, directory_issue)
		return false
	
	var server_folder = Directory.new()
	if not server_folder.dir_exists(target_dir):
		emit_signal("connect_to_server_ok_s", false, directory_issue)
		return false
	
	directory = target_dir
	
	var peer = NetworkedMultiplayerENet.new()
	if not peer.create_client(server_address, port) == OK:
		print("Failed to create client from peer.")
		emit_signal("connect_to_server_ok_s", false, connect_issue)
		return false
	get_tree().set_network_peer(peer)
	get_tree().set_meta("network_peer", peer)
	emit_signal("connect_to_server_ok_s", true, null)
	
	return true

# Loads the config file for the current server, if it exists.
func load_config_file():
	var config_file = File.new()
	if not config_file.file_exists(directory + "/" + files[files_enum.CLIENT_CONFIG]):
		return
	
	config_file.open(directory + "/" + files[files_enum.CLIENT_CONFIG], File.READ)
	while not config_file.eof_reached():
		var line = parse_json(config_file.get_line())
		if not line:
			break
		config = line

# Closes connection.
func close_connection():
	var peer = get_tree().get_meta("network_peer")
	peer.close_connection()
	clear_peer()

# Resets network_peer to null. Used for signal once connection is
# closed by the server, but also as auxiliary function.
func clear_peer():
	get_tree().set_meta("network_peer", null)

# ============================================================
# Connecting signals
# ============================================================

func setup_client_signals(connection, login):
	if login.connection_signals:
		return
	if login.connect("attempt_login", connection, "send_login_info") != 0:
		print("Failed to connect attempt_login!")
	if login.connect("attempt_register", connection, "send_register_user") != 0:
		print("Failed to connect attempt_register!")

# ============================================================
# _ready() functions for startup!
# ============================================================

func _ready():
	if get_tree().connect("server_disconnected", self, "clear_peer") != 0:
		print("Failed to connect server_disconnected to clear_peer() on Client!")