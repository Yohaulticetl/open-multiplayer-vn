extends Node

enum AUDIO { BGM, SFX }

var BGMs = {}
var SFXs = {}

var bgm_directory
var sfx_directory

const audio_extensions = ["wav", "ogg"]

# =================== Loading Functions ===================

# Call this one first so it can setup the directories and variables.
func load_bgms(main_dir):
	if not setup_directory(main_dir):
		return false
	
	var dir = Directory.new()
	dir.open(bgm_directory)
	dir.list_dir_begin()
	
	while true:
		var file = dir.get_next()
		if file == "":
			break
		
		if not file.begins_with(".") and file.get_extension() in audio_extensions:
			load_bgm(file)
	dir.list_dir_end()

func load_bgm(file):
	if bgm_file_exists(file):
		var audio_file
		if file.get_extension() == "ogg":
			audio_file = import_ogg(bgm_directory + file)
			audio_file.set_loop(true)
		elif file.get_extension() == "wav":
			audio_file = import_wav(bgm_directory + file)
			audio_file.set_loop_mode(AudioStreamSample.LOOP_FORWARD)
		BGMs[file.get_basename()] = audio_file
		return true
	return false

# This one should be called after load_bgms().
func load_sfxs():
	var dir = Directory.new()
	dir.open(sfx_directory)
	dir.list_dir_begin()
	
	while true:
		var file = dir.get_next()
		if file == "":
			break
		
		if not file.begins_with(".") and file.get_extension() in audio_extensions:
			load_sfx(file)

func load_sfx(file):
	if sfx_file_exists(file):
		var audio_file
		if file.get_extension() == "ogg":
			audio_file = import_ogg(sfx_directory + file)
		elif file.get_extension() == "wav":
			audio_file = import_wav(sfx_directory + file)
		SFXs[file.get_basename()] = audio_file
		return true
	return false

# =================== Importing Functions ===================

func import_ogg(path):
	var file = File.new()
	file.open(path, File.READ)
	var bytes = file.get_buffer(file.get_len())
	file.close()
	
	var stream = AudioStreamOGGVorbis.new()
	stream.set_data(bytes)
	return stream

func import_wav(path):
	var file = File.new()
	file.open(path, File.READ)
	var bytes = file.get_buffer(file.get_len())
	file.close()
	
	var stream = AudioStreamSample.new()
	stream.set_data(bytes)
	return stream

# =================== External Functions ===================

func get_bgm(audio_name):
	if not audio_name in BGMs:
		return null
	return BGMs[audio_name]

func get_sfx(audio_name):
	if not audio_name in BGMs:
		return null
	return SFXs[audio_name]

func get_audio(audio_name):
	if not get_bgm(audio_name):
		return get_sfx(audio_name)

func get_bgms():
	return BGMs

func get_sfxs():
	return SFXs

# =================== Initialization Functions ===================

func setup_directory(main_dir):
	if not main_dir:
		return false
	
	var dir = Directory.new()
	if not dir.dir_exists(main_dir):
		return false
	
	bgm_directory = main_dir + "/BGM/"
	sfx_directory = main_dir + "/SFX/"
	if not dir.dir_exists(bgm_directory):
		dir.make_dir(bgm_directory)
	if not dir.dir_exists(sfx_directory):
		dir.make_dir(sfx_directory)
	return true

# =================== Auxiliary Functions ===================

func audio_file_exists(audio):
	if file_exists(bgm_directory + audio):
		return true
	else:
		return file_exists(sfx_directory + audio)

func bgm_file_exists(audio):
	return file_exists(bgm_directory + audio)

func sfx_file_exists(audio):
	return file_exists(sfx_directory + audio)

func file_exists(path):
	var file = File.new()
	return file.file_exists(path)

func bgm_exists(audio):
	if audio in BGMs and BGMs[audio]:
		return true
	return false

func sfx_exists(audio):
	if audio in SFXs and SFXs[audio]:
		return true
	return false