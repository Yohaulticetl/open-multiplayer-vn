extends Panel

# Unsure whether the current path set by Godot can bug the game.
# In case it does, just set the current path for the $SelectDirectory
# manually in the code.

const invalid_directory = "Please, select a valid directory for the client to use resources from."
const invalid_username_password = "Your username or password was invalid."
const invalid_server_address_port = "Please, check the Server IP and Port before trying again."

const message_connecting = "Trying to connect to server, please wait..."
const message_connect_fail = "Failed to connect to the server."

var selected_directory
#var creation_timer

signal attempt_register(username, password, server_password)
signal attempt_login(username, password, server_password)
signal start_game

# warning-ignore:unused_class_variable
var connection_signals = false
var attempt

# Auxiliar function for the popup dialog to set the text and popup centered.
func popup_error(text):
	$PopupPanel.click_hides = false
	$PopupPanel.set_text(text)
	$PopupPanel.popup_centered()

# Creates the client and attempts connection with it.
func create_client():
	if not $ServerIP.text or not $ServerPort.text or not $ServerPort.text.is_valid_integer():
		popup_error(invalid_server_address_port)
		return false
	if not $Username.text or not $Password.text:
		popup_error(invalid_username_password)
		return false
	if not selected_directory:
		popup_server_hideable(invalid_directory)
		return false
	
	# Needs a timer and connection to the signal for connected to server, to attempt proper login.
	return NetworkManager.connect_to_server(selected_directory, int($ServerPort.text), $ServerIP.text)

# Auxiliar function for selecting the directory, used in conjunction with the signal from
# $SelectResourcesFolder.
func select_directory(directory):
	selected_directory = directory

# Called when the node enters the scene tree for the first time. Sets up every necessary signal.
func _ready():
	if $SelectDirectory.connect("button_down", $SelectResourcesFolder, "popup_centered") != 0:
		print("Failed to connect files button down to popup_centered!")
	if $SelectResourcesFolder.connect("dir_selected", $SelectDirectory, "change_label") != 0:
		print("Failed to connect dir_selected to change_label!")
	if $SelectResourcesFolder.connect("dir_selected", self, "select_directory") != 0:
		print("Failed to connect dir_selected to function select_directory!")
	if $Create.connect("button_down", self, "button_register") != 0:
		print("Failed to connect create button to button_register!")
	if $Login.connect("button_down", self, "button_login") != 0:
		print("Failed to connect login button to button_register!")
	
	if get_tree().connect("connected_to_server", self, "attempt_operation") != 0:
		print("Failed to connect attempt_operation function!")
	if get_tree().connect("connection_failed", self, "failed_connection") != 0:
		print("Failed to connect failed_connection function!")
	
	if connect("start_game", get_parent(), "start_game") != 0:
		print("Failed to connect start_game function to parent!")

func connections_signal_handling(connection):
	if connection.connect("login_ok_s", self, "receive_login_ok") != 0:
		print("Failed to connect receive_login_ok(result) to login_ok_s!")
	if connection.connect("register_ok_s", self, "receive_register_ok") != 0:
		print("Failed to connect receive_register_ok(result) to register_ok_s!")

# ============================================================
# Preliminary functions for buttons
# ============================================================

func button_register():
	if get_connection_status() == 2:
		popup_server_hideable("You are already connected to a server.")
		return false
	
	attempt = "register"
	if not create_client():
		print("Failed to connect to server.")
		return false
	message_connecting_f()
	return true

func button_login():
	if get_connection_status() == 2:
		popup_server_hideable("You are already connected to a server.")
		return false
	
	attempt = "login"
	if not create_client():
		print("Failed to connect to server.")
		return false
	message_connecting_f()
	return true

# Auxiliar function to set the PopupPanel to be hideable on click,
# and set the appropriate text.
func popup_server_hideable(new_text):
	$PopupPanel.click_hides = true
	$PopupPanel.set_text(new_text)
	$PopupPanel.popup_centered()

# ============================================================
# Functions for actual attempts
# ============================================================

func message_connecting_f():
	popup_error(message_connecting)

func failed_connection():
	popup_server_hideable(message_connect_fail)

func attempt_operation():
	if attempt == "login":
		attempt_login()
	elif attempt == "register":
		attempt_register()

func attempt_register():
	emit_signal("attempt_register", $Username.text, str($Password.text.hash()), $ServerPassword.text)

func attempt_login():
	emit_signal("attempt_login", $Username.text, str($Password.text.hash()), $ServerPassword.text)

# ============================================================
# Functions for receiving signals
# ============================================================

func receive_login_ok(result):
	if result:
		emit_signal("start_game")
		return true
	failed_connection()
	get_tree().set_network_peer(null)
	get_tree().set_meta("network_peer", null)
	return false

func receive_register_ok(result):
	if result:
		emit_signal("start_game")
		return true
	failed_connection()
	get_tree().set_network_peer(null)
	get_tree().set_meta("network_peer", null)
	return false

func get_connection_status():
	if not get_tree().has_meta("network_peer"):
		return 0
	return get_tree().get_meta("network_peer").get_connection_status()
