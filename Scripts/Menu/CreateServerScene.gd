extends Panel

# Unsure whether the current path set by Godot can bug the game.
# In case it does, just set the current path for the $ServerFilesButton
# manually in the code.

const invalid_directory = "Please, select a valid directory for the client to use resources from."
const invalid_port = "Please, check the Server port before trying again."
const invalid_max_players = "Please, check the maximum amount of players before trying again."

const server_create_success = "Server created successfully. Click anywhere to continue..."
const server_create_failure = "Failed to create server. Please, make sure there are no other servers running on this machine. Click anywhere to continue..."

const server_open_success = "Server opened successfully. Click anywhere to continue..."
const server_open_failure = "Failed to open server. Check server files and make sure no other servers are running on this machine. Click anywhere to continue..."

var connected_to_network_base = false

var selected_directory
var creation_timer

signal attempt_create_server(target_dir, port, max_players, password)
signal attempt_open_server(target_dir, port, max_players, password)
signal start_game

# ============================================================
# MISSING: Confirmation for creating a new server from scratch
# if a server config file already exists.
# ============================================================

# Auxiliar function for the popup dialog to set the text and popup centered.
func popup_error(text):
	$ErrorDialog.dialog_text = text
	$ErrorDialog.popup_centered()

# Creates the client and attempts connection with it.
func create_server():
	if not $ServerPort.text or not $ServerPort.text.is_valid_integer():
		popup_error(invalid_port)
		return false
	if not selected_directory:
		popup_error(invalid_directory)
		return false
	if not $MaximumPlayers.text or not $MaximumPlayers.text.is_valid_integer():
		popup_error(invalid_max_players)
		return false
	return true

# Auxiliar function for selecting the directory, used in conjunction with the signal from
# $SelectResourcesFolder.
func select_directory(directory):
	selected_directory = directory

# Called when the node enters the scene tree for the first time.
func _ready():
	if $ServerFilesButton.connect("button_down", $FileDialog, "popup_centered") != 0:
		print("Failed to connect files button to popup_centered!")
	if $FileDialog.connect("dir_selected", $ServerFilesButton, "change_label") != 0:
		print("Failed to connect dir_selected to change_label!")
	if $FileDialog.connect("dir_selected", self, "select_directory") != 0:
		print("Failed to connect dir_selected to function select_directory!")
	if $Create.connect("button_down", self, "attempt_create_server") != 0:
		print("Failed to connect create button to attempt_create_server!")
	
	if connect("start_game", get_parent(), "start_game") != 0:
		print("Failed to connect start_game to the parent!")

# Auxiliar function to create a one-shot timer with specific time, connect it to the appropriate
# function and start it.
func create_timer(time, timeout_function_name, args):
	if is_instance_valid(creation_timer):
		return false
	
	var result = Timer.new()
	result.set_wait_time(time)
	result.set_one_shot(true)
	add_child(result)
	
	if result.connect("timeout", self, timeout_function_name, [args]) != 0:
		result.queue_free()
		return false
	
	result.start()
	creation_timer = result
	return true

# Auxiliar function to set the PopupPanel to be hideable on click,
# and set the appropriate text.
func popup_server_timer(new_text):
	$PopupPanel.click_hides = true
	$PopupPanel.set_text(new_text)
	creation_timer.queue_free()

# =============== Server creation/opening functions ===============

func attempt_create_server():
	if not create_server():
		return false
	
	if not create_timer(30, "popup_server_timer", server_create_failure):
		print("Error: Failed to create the server creation timer.")
	if not connected_to_network_base:
		print("Error: Not connected to network base!")
	
	$PopupPanel.set_text("Setting Up Server")
	$PopupPanel.popup_centered()
	emit_signal("attempt_create_server", selected_directory, int($ServerPort.text), int($MaximumPlayers.text), $ServerPassword.text)
	return true

func attempt_open_server():
	if not create_server():
		return false
	
	if not create_timer(30, "popup_server_timer", server_open_failure):
		print("Error: Failed to create the server creation timer.")
	if not connected_to_network_base:
		print("Error: Not connected to network base!")
	
	$PopupPanel.set_text("Setting Up Server")
	$PopupPanel.popup_centered()
	emit_signal("attempt_open_server", selected_directory, int($ServerPort.text), int($MaximumPlayers.text), $ServerPassword.text)
	return true

func check_errors(result, text):
	if result:
		$PopupPanel.hide()
		emit_signal("start_game")
		return true
	
	$PopupPanel.click_hides = true
	$PopupPanel.set_text(text)
	return false

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
