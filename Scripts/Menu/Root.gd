extends Node

# Loading up scenes and scripts
var server_node = preload("res://Scenes/Menu/CreateServerScene.tscn")
var client_node = preload("res://Scenes/Menu/LoginScene.tscn")
var main_menu_node = preload("res://Scenes/Menu/MainMenu.tscn")
var game_node = preload("res://Scenes/Game/Game.tscn")

var open_menu

# Frees current open menu and opens up the server page for the menu, from where
# the player can create or open a server up.
func open_server_menu():
	if is_instance_valid(open_menu):
		open_menu.queue_free()
	
	var server_scene = server_node.instance()
	add_child(server_scene)
	server_scene.get_node("Cancel").connect("button_down", self, "return_to_main_menu")
	
	open_menu = server_scene
	NetworkManager.setup_server_signals(open_menu)
	NetworkManager.setup_connection_signals(ConnectionManager)

# Frees current open menu and opens up the login page, from where the player
# can log into a server.
func open_client_menu():
	if is_instance_valid(open_menu):
		open_menu.queue_free()
	
	var client_scene = client_node.instance()
	add_child(client_scene)
	client_scene.get_node("Cancel").connect("button_down", self, "return_to_main_menu")
	
	open_menu = client_scene
	open_menu.connections_signal_handling(ConnectionManager)
	NetworkManager.setup_client_signals(ConnectionManager, open_menu)

# Frees current open menu and starts up the game scene, from where the player
# properly plays the game and interacts with other players.
func start_game():
	if is_instance_valid(open_menu):
		open_menu.queue_free()
	
	var game_scene = game_node.instance()
	add_child(game_scene)
	
	open_menu = game_scene

# Frees current open menu and creates the child main menu, from which the user can
# go into the login menu, create a server menu or exit the game.
func return_to_main_menu():
	if is_instance_valid(open_menu):
		open_menu.queue_free()
	
	var main_menu_scene = main_menu_node.instance()
	add_child(main_menu_scene)
	if $MainMenu/Create.connect("button_down", self, "open_server_menu") != 0:
		print("Failed to connect create a server button to open_server_menu!")
	if $MainMenu/Login.connect("button_down", self, "open_client_menu") != 0:
		print("Failed to connect login button to open_client_menu!")
	if $MainMenu/Exit.connect("button_down", self, "exit_game") != 0:
		print("Failed to connect exit button to exit_game!")
	
	open_menu = main_menu_scene

# Exits the game.
func exit_game():
	get_tree().quit()

# Called when the node enters the scene tree for the first time.
func _ready():
	return_to_main_menu()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
