extends Control

var IngameMenu = load("res://Scenes/IngameMenu/IngameMenu.tscn")
var Message = load("res://Scripts/Game/Message.gd")
var MessageActor = load("res://Scripts/Game/MessageActor.gd")
var Conversation = load("res://Scripts/Character/Conversation.gd")

var bg_mode = "image"
var bg_loop = true
var main_directory

var ingame_menu

# By having the server manage the conversations we avoid the sync issues that were happening in the prototype.
var conversations = {}
var messages = []

# Last message will be used in conjunction with display_message in order to prevent attempts at displaying a message
# before the client is properly setup. It will also be sent by the server to clients when they just log in.
var last_message

var sprites = {}

var current_bgm

signal conversations_changed
signal permissions_changed

signal user_var_changed(key, value)

const window_width = 1024
const window_height = 600

const sprite_width = 204

enum USER_STATES { INGAME, INSIDE_MENU }

# =================== Initial Setup Functions ===================

func _ready():
	setup_main_directory(NetworkManager.directory)
	BackgroundManager.load_backgrounds(main_directory)
	BackgroundManager.load_videos()
	CharacterManager.load_characters(main_directory)
	PermissionManager.load_roles(main_directory)
	AudioManager.load_bgms(main_directory)
	AudioManager.load_sfxs()
	
	create_sprites()
	request_update_conversations()
	request_update_roles()
	request_user_state(USER_STATES.INGAME)
	
	$position.populate_positions()
	$expression.populate_expressions()
	$position.update_position()
	$expression.update_expression()
	
	if not connect("user_var_changed", self, "handle_user_var_changed") == OK:
		print("Failed to connect user_var_changed to self in Game!")
	if not $position.connect("item_selected", self, "position_item_selected") == OK:
		print("Failed to connect item_selected from position to Game!")
	if not $expression.connect("item_selected", self, "expression_item_selected") == OK:
		print("Failed to connect item_selected from expression to Game")

# =================== Main Screen Buttons ===================

func position_item_selected(idx):
	var id = ConnectionManager.get_id()
	var user = ConnectionManager.get_user(id)
	
	if user.get_position() == $position.get_selected():
		return
#	request_change_user_var(user.get_username(), "position", $position.get_selected())
	request_change_user_var(id, "position", $position.get_selected())

func expression_item_selected(idx):
	var id = ConnectionManager.get_id()
	var user = ConnectionManager.get_user(id)
	
	if user.get_expression() == $expression.get_current_text():
		return
#	request_change_user_var(user.get_username(), "expression", $expression.get_current_text())
	request_change_user_var(id, "expression", $expression.get_current_text())

func handle_user_var_changed(key, value):
	if key == "character":
		$expression.populate_expressions()
		$expression.update_expression()
	elif key == "expression":
		$expression.update_expression()
	elif key == "position":
		$position.update_position()

# =================== Setup Functions ===================

func setup_main_directory(dir):
	main_directory = dir

remote func set_user_state(new_state, id, random_id):
	if not is_user(id, random_id):
		return false
	
	var user = get_user(id)
	if user.get_state() == new_state:
		return false
	user.set_state(new_state)
	if get_tree().is_network_server() and not id == 1:
		rpc_id(id, "set_user_state", new_state, id, random_id)
	return true

func request_user_state(new_state):
	var id = get_id()
	var random_id = get_random_id(id)
	
	if get_tree().is_network_server():
		set_user_state(new_state, id, random_id)
		return
	rpc_id(1, "set_user_state", new_state, id, random_id)

# =================== Input Functions ===================

func _input(event):
	if event.is_action_pressed("open_menu"):
		call_ingame_menu()
	if event.is_action_pressed("send_message") and not ingame_menu:
		if $DialogueBox.all_visible and $MessageInput.has_focus():
			send_message()
		elif not $DialogueBox.all_visible:
			$DialogueBox.show_all()

# =================== Background Handling Functions ===================

func valid_change_bg_mode(type):
	if not type == "image" or not type == "video":
		print("Error: Attempt to change into inexistent background type! type == " + type + ".")
		return false
	if not type == bg_mode:
		return true
	return false

func change_bg_mode(type):
	if not valid_change_bg_mode(type):
		return false
	
	if type == "image":
		$Video_BG.hide()
		$Image_BG.show()
	
	elif type == "video":
		$Video_BG.show()
		$Image_BG.hide()

# =================== Ingame Menu Functions ===================

func call_ingame_menu():
	# Important note: It seems that by storing a reference to this node
	# the game is getting confused and attempting to call queue_free() on
	# random different resources, which is undoubtedly bad. Thus, instead
	# of storing the node's name, we'll be storing his last used name.
	# It's probably not readable by humans, but should be good enough.
	if ingame_menu == null:
		var ingame_menu_node = IngameMenu.instance()
		add_child(ingame_menu_node)
		ingame_menu_node.setup_conversations(get_user(get_id()).get_username(), conversations)
		ingame_menu = ingame_menu_node.name
	else:
		get_node(ingame_menu).queue_free()
		ingame_menu = null

# =================== Message Handling Functions ===================

func send_message():
	var message_text = $MessageInput.get_text()
	if message_text:
		$MessageInput.clear()
		var id = get_id()
		var user = get_user(id)
		
		var message = Message.new()
		message.set_text(message_text)
		message.set_display_name(user.get_alias())
		
		var actor = MessageActor.new()
		actor.get_from_user(user)
		message.add_actor(actor)
		
		if get_tree().is_network_server():
			handle_message(id, get_random_id(id), message.get_rpc_data())
		else:
			rpc_id(1, "handle_message", get_id(), get_random_id(get_id()), message.get_rpc_data())

master func handle_message(id, random_id, message):
	if not is_user(id, random_id) or not get_tree().is_network_server():
		return false
	
	if not (PermissionManager.has_permission(get_user(id), "talk", "User") or
			PermissionManager.has_permission(get_user(id), "talk", "Administration")):
		return false
	
	message.sender = get_user(id).get_username()
	# Space can be filled with message checks, profanity checks and so on.
	propagate_message(message)
	return true

puppet func receive_message(random_id, message):
	if not is_user(get_id(), random_id):
		return false
	
	# Missing proper setup of most variables present in the message
	var received_message = Message.new()
	received_message.set_rpc_data(message, MessageActor)
	
	var sender = received_message.get_sender()
	var actor_conversation = get_actor_conversation(sender)
	if not actor_conversation == null:
		update_actor_conversation(sender)
		received_message.set_actors(actor_conversation.get_actors())
	# Maybe swap this line with the function for logging received messages later.
# Removed until I know what to do. Using this will begin changing the message
# index number, meaning if you're too late you'll start missing messages. At
# the same time I have no idea how much memory we are using.
#	if messages.size() == 256:
#		messages.pop_front()
	messages.append(received_message)
	last_message = messages.size() - 1
	
	# DEBUG
	process_message(last_message)
	
	return true

func user_should_receive_message(user):
	if user.get_state() != null:
		return true
	return false

func propagate_message(message):
	if not get_tree().is_network_server():
		return false
	
	var users = get_users()
	for id in users:
		if user_should_receive_message(users[id]) and not id == 1:
			rpc_id(id, "receive_message", get_random_id(id), message)
		elif user_should_receive_message(users[id]) and id == 1:
			receive_message(get_random_id(id), message)

func process_message(number):
	if number > last_message or number < 0:
		return false
	
	var message = messages[number]
	
	$DialogueBox.show_text(message.get_text())
	$AliasBox.set_bbcode(message.get_display_name())
	show_sprites(message.get_actors())
	show_background(message.get_sender())
	set_current_bgm(message.get_sender())
	set_current_sfx(message.get_sender())

# =================== Background Handling Functions ===================

func get_background(user):
	if not get_tree().is_network_server():
		return null
	
	if user.get_background() and BackgroundManager.background_exists(user.get_background()):
		return user.get_background()
	# Should have an elif for user in conversation and conversation background
	else:
		return get_user(get_id()).get_background()

# =================== Player Request Functions ===================

func request_change_user_var(target_user, key, value):
	var id = get_id()
	var random_id = get_random_id(id)
	var user = get_user(id)
	
	if target_user == null:
		target_user = id
	
	emit_signal("user_var_changed", key, value)
#	if not target_user == user.get_username():
	if not target_user == id:
		if not PermissionManager.has_permission(user, key, "Administration"):
			return false
	else:
		if not PermissionManager.has_permission(user, key, "User"):
			return false
	
	if not get_tree().is_network_server():
		rpc_id(1, "process_change_user_var", id, random_id, target_user, key, value)
	else:
		process_change_user_var(id, random_id, target_user, key, value)
	return true

func request_edit_role(role, permission, category, value):
	if not role:
		return false
	
	var id = get_id()
	var random_id = get_random_id(id)
	
	var data = PermissionManager.get_data_copy(role)
	if not category in data.permissions:
		return false
	
	if not permission in data.permissions[category]:
		return false
	
	data.permissions[category][permission] = value
	
	if get_tree().is_network_server():
		process_edit_role(id, random_id, data)
	else:
		rpc_id(1, "process_edit_role", id, random_id, data)
	return true

func request_update_roles():
	if get_tree().is_network_server():
		return false
	
	var id = get_id()
	var random_id = get_random_id(id)
	
	rpc_id(1, "process_update_roles", id, random_id)
	return true

# =================== Variable Changed Functions ===================

puppet func user_var_changed(id, random_id, target_user, key, value):
	if not is_user(id, random_id):
		return false
	
	var users = get_users()
	if not target_user in users:
		return false
	
	users[target_user].set_variable(key, value)
	emit_signal("user_var_changed", key, value)
	
	return true

puppet func role_changed(id, random_id, role_data):
	if not is_user(id, random_id):
		return false
	
	PermissionManager.set_data(role_data)
	emit_signal("permissions_changed")
	return true

puppet func roles_updated(id, random_id, role_data):
	if not is_user(id, random_id):
		return false
	
	PermissionManager.setup_roles(role_data)
	emit_signal("permissions_changed")
	return true

# =================== Server Request Handle Functions ===================

master func process_change_user_var(id, random_id, target_user, key, value):
	if not is_user(id, random_id):
		return false
	
	if not id == target_user:
		if not PermissionManager.has_permission(get_user(id), key, "Administration"):
			return false
	else:
		if not PermissionManager.has_permission(get_user(id), key, "User"):
			return false
	
	if key == "background" and not BackgroundManager.background_exists(value) and not value == null:
		return false
	if key == "character" and not CharacterManager.character_exists(value) and not value == null:
		return false
	if key == "expression" and (not CharacterManager.expression_exists(get_user(target_user).get_character(), value)
	and not value == null):
		return false
	if key == "bgm" and not AudioManager.bgm_exists(value) and not value == null:
		return false
	if key == "sfx" and not AudioManager.sfx_exists(value) and not value == null:
		return false
	
	user_var_changed(id, random_id, target_user, key, value)
	# Will change implementation, everyone will have copies of users, but
	# the array should be indexed by username, rather than id. Don't forget
	# to not pass the password.
	
	if not id == 1:
		rpc_id(id, "user_var_changed", id, random_id, target_user, key, value)
	
	if key == "character":
		select_suitable_expression(id, random_id, target_user)

master func process_edit_role(id, random_id, role_data):
	if not is_user(id, random_id):
		return false
	
	if not PermissionManager.has_permission(get_user(id), "role", "Administration"):
		return false
	
	if not role_data.has("name") or not role_data.has("permissions"):
		return false
	
	if not PermissionManager.role_exists(role_data.name):
		return false
	
	propagate_permission_edit(role_data)

master func process_update_roles(id, random_id):
	if not is_user(id, random_id):
		return false
	
	var roles = {}
	for role in PermissionManager.get_roles().values():
		roles[role.get_name()] = role.get_data()
	
	if not id == 1:
		rpc_id(id, "roles_updated", id, random_id, roles)
	return true

# =================== Helper Functions for Processing ===================

func position_is_valid(position):
	if not typeof(position) == TYPE_INT:
		return false
	if position < 0 or position > 4:
		return false
	return true

func expression_is_valid(expression, character):
	if not expression in character.expressions:
		return false
	if character.expressions[expression].empty():
		return false
	return true

func select_suitable_expression(id, random_id, target_user):
	if not is_user(id, random_id):
		return false

	var new_char = CharacterManager.get_character(get_user(target_user).get_character())
	if not new_char:
		process_change_user_var(id, random_id, target_user, "expression", null)
	elif not new_char.data.expressions.empty():
		for expression in new_char.data.expressions:
			if expression_is_valid(expression, new_char):
				process_change_user_var(id, random_id, target_user, "expression", expression)
				break
	else:
		process_change_user_var(id, random_id, target_user, "expression", null)

func propagate_permission_edit(role_data):
	var users = get_users()
	
	for i in users:
		if not i == 1:
			rpc_id(i, "role_changed", i, get_random_id(i), role_data)
		else:
			role_changed(i, get_random_id(i), role_data)

# =================== Conversation Request Functions ===================

func request_new_conversation(conversation_name):
	var id = get_id()
	var random_id = get_random_id(id)
	
	if get_tree().is_network_server():
		process_new_conversation(id, random_id, conversation_name)
	else:
		rpc_id(1, "process_new_conversation", id, random_id, conversation_name)

func request_enter_conversation(conversation_name, position):
	var id = get_id()
	var random_id = get_random_id(id)
	var user = get_user(id)
	
	var actor = MessageActor.new()
	actor.get_from_user(user)
	actor.position = position
	
	if get_tree().is_network_server():
		process_add_actor_to_conversation(id, random_id, actor.get_rpc_data(), conversation_name)
	else:
		rpc_id(1, "process_add_actor_to_conversation", id, random_id, actor.get_rpc_data(), conversation_name)

func request_leave_conversation(conversation_name):
	var id = get_id()
	var random_id = get_random_id(id)
	var user = get_user(id)
	
	if not conversation_exists(conversation_name):
		return false
	var actor = conversations[conversation_name].get_actor(user.get_username())
	if not actor:
		return false
	
	if get_tree().is_network_server():
		process_actor_leave_conversation(id, random_id, actor.get_rpc_data(), conversation_name)
	else:
		rpc_id(1, "process_actor_leave_conversation", id, random_id, actor.get_rpc_data(), conversation_name)

func request_remove_conversation(conversation_name):
	var id = get_id()
	var random_id = get_random_id(id)
	
	if not conversation_exists(conversation_name):
		return false
	
	if get_tree().is_network_server():
		process_remove_conversation(id, random_id, conversation_name)
	else:
		rpc_id(1, "process_remove_conversation", id, random_id, conversation_name)
	return true

func request_update_conversations():
	if get_tree().is_network_server():
		return false
	
	var id = get_id()
	var random_id = get_random_id(id)
	
	rpc_id(1, "process_update_conversations", id, random_id)

func request_change_conversation_bg(conversation, background):
	var id = get_id()
	var random_id = get_random_id(id)
	
	if get_tree().is_network_server():
		process_change_conversation_bg(id, random_id, conversation, background)
	else:
		rpc_id(1, "process_change_conversation_bg", id, random_id, conversation, background)
	return true

# =================== Conversation Processing Functions ===================

master func process_new_conversation(id, random_id, conversation_name):
	if not is_user(id, random_id):
		return false
	if not valid_new_conversation(conversation_name):
		return false
	
	# Permission check
	if not PermissionManager.has_permission(get_user(id), "create_conversation", "Conversation"):
		return false
	
	# Setup variable
	create_conversation(id, random_id, conversation_name)
	var conversations_data = get_conversations_data()
	
	# Update
	propagate_conversations(conversations_data)
	return true

master func process_add_actor_to_conversation(id, random_id, actor, conversation_name):
	if not is_user(id, random_id):
		return false
	if not conversation_exists(conversation_name):
		return false
	if conversation_position_full(conversation_name, actor.position):
		return false
	
	# Permission check
	if not (PermissionManager.has_permission(get_user(id), "enter_conversation", "User") or
	PermissionManager.has_permission(get_user(id), "enter_conversation", "Administration")):
		return false
	
	# Setup variable
	add_actor_to_conversation(id, random_id, actor, conversation_name)
	var conversations_data = get_conversations_data()
	
	# Update
	propagate_conversations(conversations_data)
	return true

master func process_actor_leave_conversation(id, random_id, actor, conversation_name):
	if not is_user(id, random_id):
		return false
	if not conversation_exists(conversation_name):
		return false
	
	# Permission check
	if not (PermissionManager.has_permission(get_user(id), "leave_conversation", "User") or
	PermissionManager.has_permission(get_user(id), "leave_conversation", "Administration")):
		return false
	
	# Validation
	var new_actor = MessageActor.new()
	new_actor.set_rpc_data(actor)
	if not conversations[conversation_name].is_actor_in_conversation(new_actor):
		return false
	
	if not conversations[conversation_name].is_actor_in_position(new_actor):
		return false
	
	# Setup variable
	conversations[conversation_name].remove_actor(new_actor)
	var conversations_data = get_conversations_data()
	
	# Update
	propagate_conversations(conversations_data)
	return true

master func process_remove_conversation(id, random_id, conversation_name):
	if not is_user(id, random_id):
		return false
	if not conversation_exists(conversation_name):
		return false
	
	# Permission check
	if not PermissionManager.has_permission(get_user(id), "delete_conversation", "Conversation"):
		return false
	
	# Setup variable
	remove_conversation(id, random_id, conversation_name)
	var conversations_data = get_conversations_data()
	
	# Update
	propagate_conversations(conversations_data)
	return true

master func process_update_conversations(id, random_id):
	if not is_user(id, random_id):
		return false
	
	var conversation_data = {}
	for i in conversations:
		conversation_data[i] = conversations[i].get_rpc_data()
	
	rpc_id(id, "update_conversations", id, random_id, conversation_data)

master func process_change_conversation_bg(id, random_id, conversation, background):
	if not is_user(id, random_id):
		return false
	
	if not conversation_exists(conversation):
		return false
	
	# Permission check
	if not PermissionManager.has_permission(get_user(id), "change_background", "Conversation"):
		return false
	
	# Setup variable
	change_conversation_bg(conversation, background)
	var conversations_data = get_conversations_data()
	
	# Update
	propagate_conversations(conversations_data)
	return true

# =================== Auxiliary Conversation Functions ===================

func conversation_exists(conversation_name):
	if conversation_name in conversations:
		return true
	return false

func valid_new_conversation(conversation_name):
	if conversation_exists(conversation_name):
		return false
	
	# Space can be used for profanity checking functions.
	return true

func conversation_position_full(conversation_name, position):
	if conversations[conversation_name].is_position_full(position):
		return true
	return false

func get_actor_conversation(actor):
	for i in conversations:
		if conversations[i].is_actor_in_conversation(actor):
			return conversations[i]
	return null

# Important since new users may log in and not have all the conversations data.
# IMPORTANT:
# Missing request conversations data function for such update
func propagate_conversations(data):
	var users = get_users()
	
	for i in users:
		if not i == 1:
			rpc_id(i, "update_conversations", i, get_random_id(i), data)
		else:
			update_conversations(i, get_random_id(i), data)

func get_conversations_data():
	var data = {}
	for i in conversations:
		data[i] = conversations[i].get_rpc_data()
	
	return data

func update_conversations_menu():
	emit_signal("conversations_changed", conversations)

func change_conversation_bg(conversation, bg_name):
	if not conversation in conversations:
		return false
	
	conversations[conversation].set_background(bg_name)
	return true

# =================== Conversation Result Functions ===================

func update_actor_conversation(actor):
	for i in conversations:
		if conversations[i].is_actor_in_conversation(actor):
			var position = conversations[i].get_actor_position(actor)
			var target = conversations[i].get_position(position)
			target.set_rpc_data(actor)
			target.position = position
			target.background = conversations[i].get_background()
			return true
	return false

# These are only called on the server.
func create_conversation(id, random_id, conversation_name):
	if not is_user(id, random_id):
		return false
	
	var new_conversation = Conversation.new()
	new_conversation.name = conversation_name
	conversations[new_conversation.name] = new_conversation
	return true

func add_actor_to_conversation(id, random_id, actor, conversation_name):
	if not is_user(id, random_id):
		return false
	
	var new_actor = MessageActor.new()
	new_actor.set_rpc_data(actor)
	var previous_conversation = get_actor_conversation(new_actor)
	if not previous_conversation == null:
		previous_conversation.remove_actor(new_actor)
	
	conversations[conversation_name].add_actor(new_actor)
	return true

func leave_conversation(id, random_id, actor, conversation_name):
	if not is_user(id, random_id):
		return false
	
	conversations[conversation_name].remove_actor(actor)
	return true

func remove_conversation(id, random_id, conversation_name):
	if not is_user(id, random_id):
		return false
	
	conversations.erase(conversation_name)
	return true

# This is the one that's called on both the server and the clients.
puppet func update_conversations(id, random_id, new_conversations):
	if not is_user(id, random_id):
		return false
	
	conversations.clear()
	
	for i in new_conversations:
		var conversation = Conversation.new()
		conversation.set_rpc_data(new_conversations[i], MessageActor)
		conversations[i] = conversation
	
	emit_signal("conversations_changed", conversations)
	return true

# =================== (Character) Animated Sprite Functions ===================

func get_sprite(pos):
	return sprites[pos]

func create_sprites():
	# Order of sprites dictate which goes on top, in case of overlapping.
	# By default, middle one should be displayed on top.
	create_sprite(0)
	create_sprite(1)
	create_sprite(4)
	create_sprite(3)
	create_sprite(2)

func create_sprite(pos):
	var sprite = AnimatedSprite.new()
	$Sprites.add_child(sprite)
	sprite.position = Vector2((sprite_width * pos) + (sprite_width / 2), (window_height * 3 / 4))
	sprite.frames = SpriteFrames.new()
	sprites[pos] = sprite

func fit_sprite(sprite, actor):
	var animation = actor.expression
	if not validate_sprite(sprite, animation):
		return false
	
	var texture = sprite.get_sprite_frames().get_frame(animation, 0)
	var tex_size = texture.get_size()
	# Here, the default to fitting is three fourths of the screen height.
	var target_size = ((window_height * 9 / 10) / tex_size.y)
	
	var target = Vector2(target_size, target_size)
	sprite.set_scale(target)

func center_sprite_y(sprite, actor):
	var animation = actor.expression
	if not validate_sprite(sprite, animation):
		return false
	
	var texture = sprite.get_sprite_frames().get_frame(animation, 0)
	var tex_size = texture.get_size() * sprite.get_scale()
	# Textures will get cut off by 100 pixels, but set up so that they are more visible, since there's
	# both a textbox and a line for writing.
	var target_pos = window_height - (tex_size.y / 2)
	sprite.position = Vector2(sprite.position.x, target_pos)

func flip_sprite(sprite, actor):
	sprite.set_flip_h(actor.flip_character)

func validate_sprite(sprite, animation):
	if not animation or not sprite.get_sprite_frames():
		return false
	
	if not sprite.get_sprite_frames().has_animation(animation):
		return false
	
	if sprite.get_sprite_frames().get_frame_count(animation) == 0:
		return false
	return true

func show_sprites(message_actors):
	for child in $Sprites.get_children():
		child.hide()
	
	for actor in message_actors:
		if actor.position in sprites:
			process_message_sprite(actor, sprites[actor.position])

func process_message_sprite(actor, sprite):
	if not actor.character or not actor.expression or not CharacterManager.character_exists(actor.character):
		return
	
	sprite.frames = CharacterManager.get_character_frames(actor.character)
	if not sprite.frames.has_animation(actor.expression):
		return
	
	if actor.scale_character:
		fit_sprite(sprite, actor)
	if actor.center_character:
		center_sprite_y(sprite, actor)
	flip_sprite(sprite, actor)
	
	sprite.show()
	sprite.play(actor.expression)

# =================== (Background) Animated Sprite Functions ===================

func show_background(actor):
	if not actor:
		return
	
	if actor.background:
		if $Video_BG.is_playing() and $Video_BG.get_stream() == BackgroundManager.get_background(actor.background):
				return
	
	$Image_BG.hide()
	$Video_BG.hide()
	if $Video_BG.is_connected("finished", self, "background_loop"):
		$Video_BG.disconnect("finished", self, "background_loop")
	
	fit_background(actor.background)
	process_background(actor)

func fit_background(background):
	if not BackgroundManager.background_exists(background):
		return false
	
	if BackgroundManager.get_type(background) == "image":
		var bg_res = BackgroundManager.get_background(background)
		if not bg_res:
			return false
		
		var frames = bg_res.get_frames()
		if not frames.has_animation(bg_res.animation):
			return false
		
		if frames.get_frame_count(bg_res.animation) == 0:
			return false
		
		var texture = frames.get_frame(bg_res.animation, 0)
		var tex_size = texture.get_size()
		var target = Vector2((window_width / tex_size.x), (window_height / tex_size.y))
		
		$Image_BG.set_scale(target)

func process_background(actor):
	if not actor.background or not BackgroundManager.background_exists(actor.background):
		stop_video()
		return
	
	var background = BackgroundManager.get_background(actor.background)
	if not background:
		return
	
	if BackgroundManager.get_type(actor.background) == "image":
		stop_video()
		$Image_BG.frames = background.get_frames()
		$Image_BG.show()
		$Image_BG.play(background.animation)
	
	elif BackgroundManager.get_type(actor.background) == "video":
		stop_video()
		if bg_loop:
			$Video_BG.connect("finished", self, "background_loop")
		
		$Video_BG.set_stream(background)
		$Video_BG.show()
		$Video_BG.play()

func background_loop():
	if $Video_BG.get_stream():
		$Video_BG.play()

func stop_video():
	if $Video_BG.is_playing():
			$Video_BG.stop()

# =================== Audio Functions ===================

func set_current_bgm(actor):
	var bgm = actor.bgm
	if current_bgm == bgm:
		return
	if $BGM_Node.is_playing():
		$BGM_Node.stop()
	
	var audio = AudioManager.get_bgm(bgm)
	$BGM_Node.set_stream(audio)
	$BGM_Node.play()
	current_bgm = bgm

func set_current_sfx(actor):
	var sfx = actor.sfx
	if $SFX_Node.is_playing():
		$SFX_Node.stop()
	
	var audio = AudioManager.get_sfx(sfx)
	$SFX_Node.set_stream(audio)
	$SFX_Node.play()

# =================== Auxiliary Functions ===================

func is_user(id, random_id):
	if not random_id == get_random_id(id):
		return false
	return true

func get_user(id):
	return ConnectionManager.logged_users[id]

func set_user(id, user):
	ConnectionManager.logged_users[id].set_rpc_data(user)

func get_users():
	return ConnectionManager.logged_users

func get_id():
	return get_tree().get_network_unique_id()

func get_random_id(id):
	if id == get_id():
		return ConnectionManager.unique_id
	return ConnectionManager.random_IDs[id]
