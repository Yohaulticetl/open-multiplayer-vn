extends LineEdit

# Called when the node enters the scene tree for the first time.
func _ready():
	if not $send.connect("button_down", get_parent(), "send_message") == OK:
		print("Failed to connect button for sending messages!")
