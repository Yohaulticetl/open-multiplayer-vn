extends RichTextLabel

const normal_wait_time = 0.025
const slow_wait_time = 0.1
var all_visible = true

# Called when the node enters the scene tree for the first time.
func _ready():
	if not $timer.connect("timeout", self, "next_visible_character") == OK:
		print("Failed to connect the timer in DialogueBox to show characters!")

func start_character_timer(slow):
	if slow:
		$timer.set_wait_time(slow_wait_time)
	else:
		$timer.set_wait_time(normal_wait_time)
	$timer.start()

func show_text(new_text):
	set_visible_characters(0)
	set_bbcode(new_text)
	start_character_timer(is_current_slow())
	
	all_visible = false

func is_current_slow():
	var current_character = get_text()[get_visible_characters()]
	if (current_character == "." or current_character == "?" or current_character == ","):
		return true
	return false

func next_visible_character():
	var characters = get_visible_characters() + 1
	set_visible_characters(characters)
	
	if characters < get_total_character_count():
		start_character_timer(is_current_slow())
	else:
		all_visible = true

func show_all():
	if not $timer.is_stopped():
		$timer.stop()
	set_visible_characters(-1)
	all_visible = true