extends Resource

var text = ""
var character_name = ""
var actors = []
var sender = ""

# Thought about changing actors to conversations but that's pretty
# dumb, actors already hold their positions, and a conversation
# will already pass all the actors to the message.

# =================== Getters and Setters ===================

func get_actors():
	return actors

func set_actors(list):
	actors = list

func get_display_name():
	return character_name

func get_text():
	return text

func get_sender():
	for actor in actors:
		if actor.username == sender:
			return actor
	return null

func set_sender(username):
	sender = username

func add_actor(actor):
	if actor:
		actors.append(actor)

func set_text(new_text):
	if new_text:
		text = new_text

func set_display_name(char_name):
	if char_name:
		character_name = char_name

# =================== RPC Related Functions ===================

func get_rpc_data():
	var rpc_data = {}
	var actors_data = []
	
	rpc_data.text = text
	rpc_data.character_name = character_name
	rpc_data.sender = sender
	
	for actor in actors:
		actors_data.append(actor.get_rpc_data())
	
	rpc_data.actors = actors_data
	return rpc_data

func set_rpc_data(rpc_data, MessageActor):
	text = rpc_data.text
	character_name = rpc_data.character_name
	sender = rpc_data.sender
	for actor in rpc_data.actors:
		var new_actor = MessageActor.new()
		new_actor.set_rpc_data(actor)
		actors.append(new_actor)