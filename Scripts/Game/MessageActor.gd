extends Resource

# =================== Variables ===================

var username
var character
var expression
var position
var scale_character = true
var center_character = true
var flip_character = false
var background
var bgm
var sfx

# =================== Getters and Setters ===================

func get_from_user(user):
	username = user.get_username()
	character = user.get_character()
	expression = user.get_expression()
	position = user.get_position()
	scale_character = user.get_scale()
	center_character = user.get_center()
	flip_character = user.get_flip()
	background = user.get_background()
	bgm = user.get_bgm()
	sfx = user.get_sfx()

# =================== RPC Related Functions ===================

func get_rpc_data():
	var rpc_data = {}
	
	rpc_data.username = username
	rpc_data.character = character
	rpc_data.expression = expression
	rpc_data.position = position
	rpc_data.scale_character = scale_character
	rpc_data.center_character = center_character
	rpc_data.flip_character = flip_character
	rpc_data.background = background
	rpc_data.bgm = bgm
	rpc_data.sfx = sfx
	
	return rpc_data

func set_rpc_data(rpc_data):
	username = rpc_data.username
	character = rpc_data.character
	expression = rpc_data.expression
	position = rpc_data.position
	scale_character = rpc_data.scale_character
	center_character = rpc_data.center_character
	flip_character = rpc_data.flip_character
	background = rpc_data.background
	bgm = rpc_data.bgm
	sfx = rpc_data.sfx