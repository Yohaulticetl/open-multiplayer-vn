extends Resource

# Notes on the usage of this resource:
# The role name is loaded from the file name, ensuring its uniqueness,
# as there is a single permissions directory. Its internal structure
# works with categories and keys, which in turn works well with the
# ConfigFile structure.

# Current categories are: User, Conversation, Administration.

var data = {
	"name": "New Role",
	"permissions": {}
}

var path

# =================== File Input/Output Functions ===================

func load_file():
	if not path:
		return false
	
	var config_file = ConfigFile.new()
	if not config_file.load(path) == OK:
		return false
	
	var sections = config_file.get_sections()
	
	for section in sections:
		data.permissions[section] = {}
		
		var keys = config_file.get_section_keys(section)
		for key in keys:
			data.permissions[section][key] = config_file.get_value(section, key)
	
	return true

func save_file():
	if not path:
		return false
	
	var config_file = ConfigFile.new()
	
	for category in data.permissions:
		for permission in data.permissions[category]:
			config_file.set_value(category, permission, data.permissions[category][permission])
	
	config_file.save(path)

# =================== Category Functions ===================

func create_category(category):
	if not data.permissions.has(category) and not category == null:
		data.permissions[category] = {}
		return true
	return false

func delete_category(category):
	if data.permissions.has(category) and not category == null:
		data.permissions.erase(category)
		return true
	return false

# =================== Permission Functions ===================

func add_permission(permission, category, value):
	if category == null or permission == null:
		return false
	
	if not data.permissions.has(category):
		create_category(category)
	
	data.permissions[category][permission] = value
	return true

func edit_permission(permission, category, value):
	if category == null or permission == null:
		return false
	
	if not data.permissions.has(category):
		return false
	
	if not data.permissions[category].has(permission):
		return false
	
	data.permissions[category][permission] = value
	return true

func delete_permission(permission, category):
	if permission == null or category == null:
		return false
	
	if data.permissions.has(category):
		if data.permissions[category].has(permission):
			data.permissions[category].erase(permission)
			return true
	return false

func has_permission(permission, category):
	if permission == null or category == null:
		return false
	
	if data.permissions.has(category):
		if data.permissions[category].has(permission):
			return data.permissions[category][permission]
	return false

func permission_exists(permission, category):
	if permission == null or category == null:
		return false
	
	if data.permissions.has(category):
		return data.permissions[category].has(permission)
	return false

# =================== Setter/Getters ===================

# Name:
func set_name(name):
	data.name = name

func get_name():
	return data.name

# Path:
func set_path(new_path):
	path = new_path

func get_path():
	return path

# Data:
func set_data(new_data):
	data = new_data

func get_data():
	return data

# =================== Functions for External Stuff ===================

func copy_data():
	var data_copy = data.duplicate(true)
	return data_copy