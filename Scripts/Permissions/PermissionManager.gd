extends Node

var Role = load("res://Scripts/Permissions/Role.gd")

var roles = {}
var permissions_directory

# =================== Default Permissions ===================

# Those are the permissions players have upon themselves.
# Meaning, setting their own aliases, characters, expressions,
# changing those freely.
# Category: User
const user_permissions = [
	"alias", "character", "expression", "background", "position",
	"enter_conversation", "leave_conversation", "talk", "flip_sprite",
	"bgm", "sfx"
]

# Those are the permissions have in relation to the conversations,
# which are stored by the server.
# Category: Conversation
const conversation_permissions = [
	"create_conversation", "delete_conversation",
	"change_background"
]

# Those are the permissions a player may have upon other players,
# such as setting their aliases, character, expressions and so on.
# Category: Administration
const admin_permissions = [
	"alias", "character", "expression", "background", "position",
	"enter_conversation", "leave_conversation", "talk", "role", "flip_sprite",
	"bgm", "sfx"
]

# =================== General Roles I/O Functions ===================

func load_roles(main_dir):
	if not setup_permissions_directory(main_dir):
		return false
	
	var dir = Directory.new()
	dir.open(permissions_directory)
	dir.list_dir_begin()
	
	while true:
		var file = dir.get_next()
		if file == "":
			break
		
		if not file.begins_with(".") and file.get_extension() == "cfg":
			var role_name = file.get_basename()
			roles[role_name] = Role.new()
			roles[role_name].set_name(role_name)
			roles[role_name].set_path(permissions_directory + file)
			roles[role_name].load_file()
	dir.list_dir_end()
	
	# Additional default role, for ease of use. Non essential.
	if roles.empty():
		create_role("GM", true)
	
	# Default roles, one for guest, another for the server itself.
	if not roles.has("Guest"):
		create_role("Guest")
	
	if not roles.has("Server"):
		create_role("Server", true)

func save_roles():
	for role in roles.values():
		role.save_file()

func save_role(role_name):
	if role_name in roles:
		roles[role_name].save_file()

func create_role(role_name, default = false):
	if not role_name:
		return
	
	var role = Role.new()
	role.set_name(role_name)
	role.set_path(permissions_directory + role_name + ".cfg")
	roles[role.get_name()] = role
	
	role.create_category("User")
	role.create_category("Conversation")
	role.create_category("Administration")
	
	for permission in user_permissions:
		role.add_permission(permission, "User", default)
	for permission in conversation_permissions:
		role.add_permission(permission, "Conversation", default)
	for permission in admin_permissions:
		role.add_permission(permission, "Administration", default)
	
	role.save_file()

# =================== User Permission Functions ===================

func permission_exists(user, permission, category):
	var role = user.get_role()
	if not roles.has(role):
		return false
	
	return roles[role].permission_exists(permission, category)

func has_permission(user, permission, category):
	var role = user.get_role()
	if not roles.has(role):
		return false
	
	return roles[role].has_permission(permission, category)

func get_role(role):
	if not role in roles:
		return null
	return roles[role]

func get_roles():
	return roles

func set_role(user, role):
	if not role in roles:
		return false
	
	user.set_role(role)
	return true

func set_data(data):
	roles[data.name].set_data(data)

func get_data_copy(role):
	if not role in roles:
		return null
	
	return roles[role].copy_data()

func role_exists(role):
	return roles.has(role)

func setup_roles(role_data):
	for data in role_data.values():
		if role_exists(data.name):
			set_data(data)
		else:
			create_role(data.name)
			set_data(data)

# =================== Initialization Functions ===================

func setup_permissions_directory(main_dir):
	if not main_dir:
		return false
	var dir = Directory.new()
	if not dir.dir_exists(main_dir):
		return false
	
	permissions_directory = main_dir + "/Permissions/"
	if not dir.dir_exists(permissions_directory):
		dir.make_dir(permissions_directory)
	return true