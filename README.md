**DO NOT USE IT AS IS:** Even the most basic security features have not been implemented. In fact, no security features have been implemented. Opening a port and having people connect is a major security risk. Get at the very least something against DDoS or spam into the code, plus end-to-end encryption, before playing it.  
  
## Note  
  
In the last few months I have been able to work professionally as a developer. While I thought this would stop my work on this project, I have found myself unable to do so. It is, however, under heavy refactoring as several key functions needed to be decoupled and security measures implemented.

The current code is heavily outdated and most features will be remade using what I've learned both here and on the job. Godot's signal system, in particular, was extremely underused and should help create a much cleaner code. Threaded loading will also be implemented to make a loading screen possible. Much of the data that is loaded will be set apart, each into its own Resource.
  
Overall, my main problems with the current implementation is that back then, I crammed too much into single script files. That was mostly due to my lack of experience on designing this sort of system, something visible by the fact that this is functional yet hard to maintain. In particular, there is a lot of code repetition. Future work should go much more smoothly, once it's properly overhauled.
  
# Open Multiplayer VN
  
This project seeks to implement all the basic features present in a standard Visual Novel, while allowing the user to play in a multiplayer roleplaying environment. It uses Godot 3.1 as its engine of choice. Currently runs using GLES2 for maximum compatibility.  
  
## Features
  
### What is present  
  

* Usage of characters with their expressions (.png images)
* Usage of animated characters through .png sequences (automatically detects those with the name ending in _01.png, or whatever is the number of the frame)
* Usage of backgrounds and animated backgrounds (including .bmp, .jpg, .png, sequences of images)
* Usage of videos, especially background videos (.webm and .ogv formats)
* Usage of background music (.wav and .ogg music)
* Displaying multiple characters on the screen at one time
* Permission settings based on roles
* Sending and receiving text, with the appropriate settings above

### What is missing

* Basic security features
* Administration features (such as banning, muting one specific player and so on)
* File sharing for automatic updating of resources based on the server
* Proper sound effect setup
* Features for proper tabletop RPG playing, such as character sheets and dice rolling
* Control for the animation speed of sprites (frames per second)

  
## Usage  
  
This project was made with ease of usage in mind. If you run it for the first time, you can get the appropriate subfolders set up by creating a server and pointing it to an empty folder. This'll get you the backgrounds folder and the characters folder.  
  
### Backgrounds  
  
Just drag and drop the images you want to use into the folder and you're done. It supports .bmp, .jpg and .png.  
  
### Characters  
  
It's a fancy name for categories. If you create a subfolder called John inside the characters folder, you'll get the John character into the menu. If you put .png images there, they can then be used (they're called expressions). That means if you create a subfolder called monsters and fill it with sprites for different monsters, it'll still work normally.  
  
### Videos  
  
Videos can be used as background (with little more code they could be used as cutscenes by force hiding everything on the screen). It supports .webm and .ogv (Theora).  
**Important note**: it may complain a bit, depending on the encoding. Try it out with a different one if it doesn't work. This is an issue with the engine.
  
### BGM  
  
Short for Background Music, if you set it to play you can just keep it looping. The formats supported are .wav and .ogg (Vorbis). Ogg files are recommended.  
  
### SFX  
  
Short for sound effects, they can be made to loop, but are mainly short sounds meant to be played once. The supported formats are the same as the ones for BGM. Wav files are recommended.  
  
## Playing  
  
First, players have to connect to the server using an IP and port. If you're making a solo server for testing, set the maximum number of players to 0.  
  
By right clicking, a menu will open that will allow you to change the character, background, expression, position on the screen, background music, have a sound effect and join a group of characters that will be show on the screen at the same time.  
  
On the lower part of the screen, there is a line where you can input text. By typing things there and then pressing enter (or the button to its side), a message will be sent with all the current data of the player. That includes all the things related to the character, meaning it'll be shown with the selected expression, position and text. If more characters are meant to appear on the screen together, they will be shown with the last expression (png image) used.  
  
The name used for a group of characters meant to appear together at a single time is a conversation. You can set a specific background for them, a specific BGM and select your position through the buttons on the menu.  